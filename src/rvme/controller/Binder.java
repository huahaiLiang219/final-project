/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvme.file;

import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.TableView;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Shadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import rvme.data.SubRegionForTable;
import rvme.gui.SubRegionDialog;
import rvme.gui.Workspace;
import saf.AppTemplate;

/**
 *
 * @author YinqianZheng
 */
public class Binder {
    Group gp;
    TableView<SubRegionForTable> tb;
    final private Shadow highlight = new Shadow(1, Color.GREEN);
    Polygon currentPlg, prePlg;
    AppTemplate app;
    
    public Binder(Group gp, AppTemplate app){
        this.app = app;
        this.gp = gp;
        this.tb = ((Workspace)app.getWorkspaceComponent()).getTable();
        setUpRelation();
    }
    
    private void setUpRelation(){
        for (Node plg: gp.getChildren()){
            ((Polygon)plg).setOnMouseClicked(clicked);
        }
    }
    
    
    EventHandler<MouseEvent> clicked = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            if (prePlg!=null)
                prePlg.setEffect(null);
            
            currentPlg = (Polygon)event.getSource();
            
            if (currentPlg!=null)
                prePlg = currentPlg;
            
            
            currentPlg.setEffect(highlight);
            
            int index = gp.getChildren().indexOf(event.getSource());
            tb.getSelectionModel().select(index);
        
        
            if (event.getClickCount()==2){
                SubRegionDialog sbd=  new SubRegionDialog(app);
                ((Workspace)app.getWorkspaceComponent()).setSBD(sbd);
                
                ((Workspace)app.getWorkspaceComponent()).getController().processEditItem(sbd);
            }
        
        }
    };
    
    
}