package rvme.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

import rvme.data.DataManager;
import rvme.gui.Workspace;
import saf.AppTemplate;
import rvme.PropertyType;
import rvme.data.DesignMap;
import rvme.data.SubRegion;
import rvme.data.SubRegionForTable;
import rvme.data.ToDoItem;
import rvme.file.FileManager;
import rvme.gui.DimensionsDialog;
import rvme.gui.NewMapDialog;
import rvme.gui.SubRegionDialog;
import static saf.settings.AppStartupConstants.PATH_WORK;

/**
 * This class responds to interactions with todo list editing controls.
 * 
 * @author McKillaGorilla && huahai liang
 * @version 1.0
 */
public class MapController {
    AppTemplate app;
    
    public MapController(AppTemplate initApp) {
	app = initApp;
    }
    
    public void processAddItem() {	
	// ENABLE/DISABLE THE PROPER BUTTONS
	Workspace workspace = (Workspace)app.getWorkspaceComponent();
        
        AddItemForm aif= new AddItemForm();
    
        aif.showTable();
         ToDoItem td =  new ToDoItem();
         if(!aif.getCategory().equals("")){
             td.setCategory(aif.getCategory());
         }
         if(!aif.getDescription().equals("")){
             td.setDescription(aif.getDescription());
         }
         if(aif.getStartDate()!=null){
             td.setStartDate(aif.getStartDate());
         }
         if(aif.getEndDate()!=null){
             td.setEndDate(aif.getEndDate());
         }
         td.setCompleted(aif.getCompleted());
        
        if(aif.getSelection().equals(aif.props.getProperty(PropertyType.YES))){
//            ToDoItem tdi =  new ToDoItem(aif.getCategory(),aif.getDescription(),aif.getStartDate(),aif.getEndDate(),aif.getCompleted());
            ((DataManager)app.getDataComponent()).addItem(td);
            app.getGUI().markFileAsNotSaved();
                app.getGUI().updateToolbarControls(false);
                
            
            //foolproof  design
            Pane pane =(Pane)((VBox)(workspace.getWorkspace().getChildren().get(2))).getChildren().get(1);
            pane.getChildren().get(1).setDisable(false);
            
            if(((DataManager)app.getDataComponent()).getItems().size()>1){
                pane.getChildren().get(2).setDisable(false);
                pane.getChildren().get(3).setDisable(false);
            }
            
        }
    }
    
    /**
     *
     * 
     */
    public void processRemoveItem() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        
        TableView<ToDoItem> table = ((TableView)((VBox)(
                workspace.getWorkspace().getChildren().get(2))).getChildren().get(2));
        ToDoItem itemSelected = (ToDoItem) table.getSelectionModel().getSelectedItem();
        ((DataManager)app.getDataComponent()).getItems().remove(itemSelected);
        
        
        //foolproof  design
        Pane pane =(Pane)((VBox)(workspace.getWorkspace().getChildren().get(2))).getChildren().get(1);
            pane.getChildren().get(1).setDisable(false);
            
            if(((DataManager)app.getDataComponent()).getItems().size()>1){
                pane.getChildren().get(2).setDisable(false);
                pane.getChildren().get(3).setDisable(false);
            }
            if(((DataManager)app.getDataComponent()).getItems().size()==1){
                pane.getChildren().get(2).setDisable(true);
                pane.getChildren().get(3).setDisable(true);
            }
        if(((DataManager)app.getDataComponent()).getItems().isEmpty()){
            pane.getChildren().get(1).setDisable(true);
        }
        app.getGUI().markFileAsNotSaved();
                app.getGUI().updateToolbarControls(false);
    }
    
    public void processMoveUpItem() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        
        TableView<ToDoItem> table = ((TableView)((VBox)(
                workspace.getWorkspace().getChildren().get(2))).getChildren().get(2));
        int selectedIndex = table.getFocusModel().getFocusedIndex();
        
         Pane pane =(Pane)((VBox)(workspace.getWorkspace().getChildren().get(2))).getChildren().get(1);
        
        if (selectedIndex > 0){ 
            ToDoItem preItem = ((DataManager)app.getDataComponent()).getItems().get(selectedIndex-1);
            ((DataManager)app.getDataComponent()).getItems().remove(preItem);
            ((DataManager)app.getDataComponent()).getItems().add(selectedIndex, preItem); 
            pane.getChildren().get(3).setDisable(false);
        }
        
        //foolproof  design
        else if(selectedIndex == 0){
           
            if(((DataManager)app.getDataComponent()).getItems().size()>1){
                pane.getChildren().get(2).setDisable(true);
                pane.getChildren().get(3).setDisable(false);
            }
        }
        
        app.getGUI().markFileAsNotSaved();
                app.getGUI().updateToolbarControls(false);
        
    }
    
    public void processMoveDownItem() {
         Workspace workspace = (Workspace)app.getWorkspaceComponent();
        TableView<ToDoItem> table = ((TableView)((VBox)(
                workspace.getWorkspace().getChildren().get(2))).getChildren().get(2));
            Pane pane =(Pane)((VBox)(workspace.getWorkspace().getChildren().get(2))).getChildren().get(1);
            
        int selectedIndex = table.getFocusModel().getFocusedIndex();
        if (selectedIndex < ((DataManager)app.getDataComponent()).getItems().size()-1){ 
            ToDoItem afterItem = ((DataManager)app.getDataComponent()).getItems().get(selectedIndex+1);
            ((DataManager)app.getDataComponent()).getItems().remove(afterItem);
            ((DataManager)app.getDataComponent()).getItems().add(selectedIndex, afterItem); 
            pane.getChildren().get(2).setDisable(false);
        }
         

        //foolproof  design
           
        else if(selectedIndex == ((DataManager)app.getDataComponent()).getItems().size()-1){
            if(((DataManager)app.getDataComponent()).getItems().size()>1){
                pane.getChildren().get(3).setDisable(true);
                 pane.getChildren().get(2).setDisable(false);
            }
        }
        app.getGUI().markFileAsNotSaved();
                app.getGUI().updateToolbarControls(false);
    }
    
    public void processEditItem(SubRegionDialog sbd) {
        // ENABLE/DISABLE THE PROPER BUTTONS
	Workspace workspace = (Workspace)app.getWorkspaceComponent();
        
        ObservableList<Node>  splitpane=
            ((SplitPane)(((VBox)(workspace.getWorkspace().getChildren().get(1))).getChildren().get(0))).getItems();
        TableView<SubRegionForTable> itemsTable = (TableView<SubRegionForTable>) ((Pane)splitpane.get(1)).getChildren().get(0);
        SubRegionForTable subRegionForTable=  itemsTable.getSelectionModel().getSelectedItem();
       
        sbd.setRegionName(subRegionForTable.getNameForTable());
        sbd.setCapital(subRegionForTable.getCapitalForTable());
        sbd.setLeaderName(subRegionForTable.getLeaderForTable());
        
        int selectIndex=itemsTable.getSelectionModel().getSelectedIndex();
        sbd.currentIndex=selectIndex;
        sbd.showDialog();
        if((sbd.props.getProperty(PropertyType.YES)).equals(sbd.getSelection())){
            subRegionForTable=  itemsTable.getSelectionModel().getSelectedItem();
            subRegionForTable.setNameForTable(sbd.getRegionName());
            subRegionForTable.setCapitalForTable(sbd.getCapital());
            subRegionForTable.setLeaderForTable(sbd.getLeaderName());
            
            if(sbd.currentIndex==selectIndex){
            }else{
//                for(int i= 0; i<itemsTable;i++){
//                     regionName.setText(tableList.get(i).getName());
//                     capital.setText(tableList.get(i).getCapital());
//                     leaderName.setText(tableList.get(i).getLeader());
//                    currentIndex=currentIndex+1;
             }
            }
            
            app.getGUI().markFileAsNotSaved();
                app.getGUI().updateToolbarControls(false);
                app.getGUI().updateExportButton(false);
        
        itemsTable.refresh();
        itemsTable.getSelectionModel().clearSelection();
    }
    /*
    public void processEditItem() {
        // ENABLE/DISABLE THE PROPER BUTTONS
	Workspace workspace = (Workspace)app.getWorkspaceComponent();
        
        ObservableList<Node>  splitpane=
            ((SplitPane)(((VBox)(workspace.getWorkspace().getChildren().get(1))).getChildren().get(0))).getItems();
        TableView<SubRegionForTable> itemsTable = (TableView<SubRegionForTable>) ((Pane)splitpane.get(1)).getChildren().get(0);
        
        SubRegionForTable subRegionForTable=  itemsTable.getSelectionModel().getSelectedItem();
        
        
        SubRegionDialog sbd=  new SubRegionDialog(
                subRegionForTable.getNameForTable(),
                subRegionForTable.getCapitalForTable(),
                subRegionForTable.getLeaderForTable(),
                app
        );
        int selectIndex=itemsTable.getSelectionModel().getSelectedIndex();
        sbd.currentIndex=selectIndex;
        
//        ((DataManager)app.getDataComponent()).Next.setOnAction(e -> {
//             DesignMap designMap=((DataManager)app.getDataComponent()).getDesignMap();
//            System.out.println("1");
//            ArrayList<SubRegion> tableList = designMap.getTableList();
//            if(sbd.currentIndex<tableList.size()-1){
//            for(int i= 0; i<tableList.size();i++){
//                if(i==(sbd.currentIndex+1)){
//                    
//                     sbd.regionName.setText(tableList.get(i).getName());
//                     sbd.capital.setText(tableList.get(i).getCapital());
//                     sbd.leaderName.setText(tableList.get(i).getLeader());
//                }
//             }
//                    sbd.currentIndex=sbd.currentIndex+1;
//            }
//            
//        });
//        
//        ((DataManager)app.getDataComponent()).Previous.setOnAction(e -> {
//            DesignMap designMap=((DataManager)app.getDataComponent()).getDesignMap();
//            System.out.println("2");
//            ArrayList<SubRegion> tableList = designMap.getTableList();
//            if(sbd.currentIndex>0){
//            for(int i= 0; i<tableList.size();i++){
//                if(i==(sbd.currentIndex-1)){
//                    System.out.println(tableList.get(i).getName());
//                    
//                     sbd.regionName.setText(tableList.get(i).getName());
//                     sbd.capital.setText(tableList.get(i).getCapital());
//                     sbd.leaderName.setText(tableList.get(i).getLeader());
//                    sbd.currentIndex=sbd.currentIndex-1;
//                }
//             }
//            }
//        });
        
        
        sbd.showDialog();
        if((sbd.props.getProperty(PropertyType.YES)).equals(sbd.getSelection())){
            subRegionForTable.setNameForTable(sbd.getRegionName());
            subRegionForTable.setCapitalForTable(sbd.getCapital());
            subRegionForTable.setLeaderForTable(sbd.getLeaderName());
            
            if(sbd.currentIndex==selectIndex){
            }else{
//                for(int i= 0; i<itemsTable;i++){
//                     regionName.setText(tableList.get(i).getName());
//                     capital.setText(tableList.get(i).getCapital());
//                     leaderName.setText(tableList.get(i).getLeader());
//                    currentIndex=currentIndex+1;
             }
            }
            
            app.getGUI().markFileAsNotSaved();
                app.getGUI().updateToolbarControls(false);
                app.getGUI().updateExportButton(false);
        
        itemsTable.refresh();
            itemsTable.getSelectionModel().clearSelection();
    }
*/
    public void processDisableButton() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        TableView<ToDoItem> table = ((TableView)((VBox)(
                workspace.getWorkspace().getChildren().get(2))).getChildren().get(2));
        int selectedIndex = table.getFocusModel().getFocusedIndex();
        
        Pane pane =(Pane)((VBox)(workspace.getWorkspace().getChildren().get(2))).getChildren().get(1);
        if(selectedIndex>=0){pane.getChildren().get(1).setDisable(false);}
        
        
        if(((DataManager)app.getDataComponent()).getItems().size()>1){
        
          if(selectedIndex == 0){
                pane.getChildren().get(2).setDisable(true);
                pane.getChildren().get(3).setDisable(false);
                
        }
          else if(selectedIndex == ((DataManager)app.getDataComponent()).getItems().size()-1){
                pane.getChildren().get(2).setDisable(false);
                pane.getChildren().get(3).setDisable(true);
                
        }else{
                pane.getChildren().get(2).setDisable(false);
                pane.getChildren().get(3).setDisable(false);
          }
        }else {
                pane.getChildren().get(2).setDisable(true);
                pane.getChildren().get(3).setDisable(true);
        
        }
    }

    public void processAddImage() {
        System.out.println("add");
    }

    public void processRemoveImage() {
        System.out.println("remove");
    }

    public void processChangeMapName() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        DesignMap designMap=((DataManager)app.getDataComponent()).getDesignMap();
        
        NewMapDialog nmd= new NewMapDialog(app);
        
        String oldRegionName=designMap.getRegionName();
        String parentDir=designMap.getParentDirectory();
        String newParentDir= parentDir.substring(0, parentDir.lastIndexOf("/"));
        
        String dataFile = designMap.getPolygonsPath();
            
        nmd.setDialogValues(oldRegionName, parentDir, dataFile);
        nmd.SetFileChosserDisable();
        
        nmd.showDialog();
          if((props.getProperty(saf.PropertyType.YES)).equals(nmd.getSelection())){
              designMap.setRegionName(nmd.getRegionName());
              designMap.setParentDirectory(newParentDir+"/"+designMap.getRegionName());
              FileManager fileManager = new FileManager();
              File oldNameFile= new File(PATH_WORK+oldRegionName+".rvme");
              File newNameFile= new File(PATH_WORK+designMap.getRegionName()+".rvme");
            try {
                fileManager.saveData(((DataManager)app.getDataComponent()), newNameFile);
            } catch (IOException ex) {
                Logger.getLogger(MapController.class.getName()).log(Level.SEVERE, null, ex);
            }
              if(oldNameFile.exists()){
                  oldNameFile.delete();
              }
            }
    }

    public void processChangeBorderColorButton() {
        System.out.println("border color");
    }

    public void processChangeBorderThicknessButton() {
        System.out.println("thickness");
    }

    public void processPlayAnthemButton() {
        System.out.println("play");
    }


    public void processReassighMapColorsButton() {
        System.out.println("Random map color");
    }

    public void processChangeMapDimensionsButton() {
        System.out.println("map 3D");
        DimensionsDialog dimensionsDialog= new DimensionsDialog();
        dimensionsDialog.showDialog();
        
        if((dimensionsDialog.props.getProperty(PropertyType.YES)).equals(dimensionsDialog.getSelection())){
//            double h= dimensionsDialog.getHeight();
            double weight = Double.parseDouble(dimensionsDialog.getWidth());
            double height = Double.parseDouble(dimensionsDialog.getHeight());
            DesignMap designMap=((DataManager)app.getDataComponent()).getDesignMap();
            designMap.setWidth(weight);
            designMap.setHeight(height);
            System.out.println(weight);
            System.out.println(height);
            ((StackPane)((DataManager)app.getDataComponent()).getLeftp()).setPrefHeight(height);
            ((StackPane)((DataManager)app.getDataComponent()).getLeftp()).heightProperty().isEqualTo((int)height);
            
             app.getGUI().updateToolbarControls(false);
                app.getGUI().updateExportButton(false);
        }
    }

    public void processChangeBackgroundColors() {
        System.out.println("BackgroundColors");
        Pane st = ((DataManager)app.getDataComponent()).getMapPane();
        
        
        
 //            st.backgroundProperty().bind(backgoundColorPicker.valueProperty().);
        
    }

    public void processChangeBorderColors() {
        System.out.println("BorderColors");
    }
    
}
