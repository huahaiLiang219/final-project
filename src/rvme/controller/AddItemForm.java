/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvme.controller;


import java.time.LocalDate;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import rvme.PropertyType;

/**
 *
 * @author Huahai Liang
 */
public class AddItemForm {
    PropertiesManager props;
    
    Button yesButton;
    Button cancelButton;
    
    Label lCategory;
    Label lDescription;
    Label lStartDate;        
    Label lEndDate;    
    Label lCompleted;        
            
    TextField category ;
    TextField description ;
    DatePicker startDate ;
    DatePicker endDate;        
    CheckBox completed;
    
    public  String selection;
    
    public AddItemForm(){
                 category = new TextField();
                 description = new TextField();
                 startDate =  new DatePicker();
                 endDate =  new DatePicker();
                 completed = new CheckBox();
                 
    }
    public AddItemForm(String category,String description,LocalDate startDate,LocalDate endDate,boolean completed){

        this.category= new  TextField(category);
        this.description= new  TextField(description);
        this.startDate =  new DatePicker(startDate);
        this.endDate=  new DatePicker(endDate);
        this.completed = new CheckBox();
        this.completed.setSelected(completed);
    }

    public void showTable(){
                props = PropertiesManager.getPropertiesManager();
                final Stage dialog = new Stage();
                dialog.initModality(Modality.APPLICATION_MODAL);
                //dialog.initOwner(primaryStage);
                
                GridPane gridPane = new GridPane();

                //make the add items table
                 yesButton =new Button(props.getProperty(PropertyType.YES));
                 cancelButton =new Button(props.getProperty(PropertyType.CANCEL));
                
                 
//                 lCategory=  new Label(props.getProperty(PropertyType.CATEGORY_COLUMN_HEADING));
//                 lDescription=  new Label(props.getProperty(PropertyType.DESCRIPTION_COLUMN_HEADING));
//                 lStartDate=  new Label(props.getProperty(PropertyType.START_DATE_COLUMN_HEADING));
//                 lEndDate=  new Label(props.getProperty(PropertyType.END_DATE_COLUMN_HEADING));
//                 lCompleted=  new Label(props.getProperty(PropertyType.COMPLETED_COLUMN_HEADING));
                
                 
                
                
                gridPane.add(lCategory, 0, 0);
                gridPane.add(category, 1, 0);
                
                gridPane.add(lDescription, 0, 1);
                gridPane.add(description, 1, 1);
                
                gridPane.add(lStartDate, 0, 2);
                gridPane.add(startDate, 1, 2);
                
                gridPane.add(lEndDate, 0, 3);
                gridPane.add(endDate, 1, 3);
                
                gridPane.add(lCompleted, 0, 4);
                gridPane.add(completed, 1, 4);
                
                gridPane.add(yesButton, 0, 5);
                gridPane.add(cancelButton, 1, 5);

                GridPane.setRowSpan(yesButton, 2);
                GridPane.setRowSpan(cancelButton, 2);
                 yesButton.setOnAction(e->{
                     selection=props.getProperty(PropertyType.YES);
                     dialog.close();
                         });
                 cancelButton.setOnAction(e->{
                     selection=props.getProperty(PropertyType.CANCEL);
                     dialog.close();});
        
                 //                gridPane.getStyleClass().add("grid_pane");
                        
                
//                 
                 
               Scene dialogScene = new Scene(gridPane, 350, 300);
               //dialogScene.getStylesheets().add("tdlm/css/tdlm_style.css");
               gridPane.getStylesheets().add(props.getProperty(PropertyType.CSS_PATH));
               gridPane.getStyleClass().add(props.getProperty(PropertyType.GRID_PANE));
               
               //gridPane.setPadding(new Insets(20, 0, 20, 20));
	        //gridPane.setHgap(7); gridPane.setVgap(7);
                
                //set Label style from css file
                setLabelStyle(lCategory);
                setLabelStyle(lDescription);
                setLabelStyle(lStartDate);
                setLabelStyle(lEndDate);
                setLabelStyle(lCompleted);

               dialog.setTitle(props.getProperty(PropertyType.TABLE_TITLE));
                dialog.setScene(dialogScene);
                dialog.showAndWait();
                
               
    }
    public void setSelection(String sel) {
        this.selection=sel;
    }
    public String getSelection() {
        return selection;
    }
    
    public String getCategory(){
        return category.getText();
    }
    
    public String getDescription(){
        return description.getText();
    }
    
    public LocalDate getStartDate(){
        return startDate.getValue();
    }
    
    public LocalDate getEndDate(){
        return endDate.getValue();
    }
   
    public boolean getCompleted(){
        return completed.isSelected();
    }
    
    public void setLabelStyle(Label label){
        label.getStylesheets().add(props.getProperty(PropertyType.CSS_PATH));
        label.getStyleClass().add(props.getProperty(PropertyType.FONT));
    }
}
