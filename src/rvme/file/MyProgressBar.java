/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo;

import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.ProgressBar;

/**
 *
 * @author yusun
 */
public class MyProgressBar extends ProgressBar{

  
    double value = 0;
    IProgressCallBack callback;
    
    public MyProgressBar(IProgressCallBack c) {
        
        callback = c;
        show();
    }
    
    void updateProgress(){
        
        value+=0.2;
        if(value<=1){
            setProgress(value);
        }else{
            close();
        }
    }
    
    public void show() {
        if(callback!=null)
            try{
        callback.beforeShow();
            }catch(UnsupportedOperationException e){
                
            }
        setProgress(value);
        new Thread(new Runnable(){

            @Override
            public void run() {
               
                int counter = 0;
                while(counter++<10){
                    long time = 1000;
                    try {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                
                                updateProgress();
                            }
                        });
                        Thread.sleep(time);
                        counter++;
                    } catch (InterruptedException ex) {
                        Logger.getLogger(MyProgressBar.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            
        }).start();
    }
    
    public void close(){
        
        if(callback!=null)
        callback.finish();
    }
    
    
    
    public interface IProgressCallBack{
        
        void beforeShow(Object... obj);
        void finish(Object... obj);
    }
    
    
}
