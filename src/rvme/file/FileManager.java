package rvme.file;

import com.alibaba.fastjson.JSON;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableRow;
import javafx.scene.effect.Shadow;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import javax.imageio.ImageIO;
import javax.json.Json;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import org.apache.commons.io.FileUtils;
import properties_manager.PropertiesManager;
import rvme.data.CountryJSON;
import rvme.data.DataManager;
import rvme.data.DesignMap;
import rvme.data.SubRegion;
import rvme.data.SubRegionForTable;
import rvme.data.SubRegions;
import rvme.data.World;
import rvme.data.subRegionForWorld;
import rvme.gui.SubRegionDialog;
import rvme.gui.Workspace;
import static saf.settings.AppStartupConstants.PATH_WORK;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;
import static saf.settings.AppPropertyType.SAVE_COMPLETED_MESSAGE;
import static saf.settings.AppPropertyType.SAVE_COMPLETED_TITLE;
import saf.ui.AppMessageDialogSingleton;

/**
 * This class serves as the file management component for this application,
 * providing all I/O services.
 *
 * @author Richard McKenna
 * @version 1.0
 */
public class FileManager implements AppFileComponent {
    // FOR JSON SAVING AND LOADING
    static final String JSON_CATEGORY = "category";
    static final String JSON_DESCRIPTION = "description";
    static final String JSON_START_DATE = "start_date";
    static final String JSON_END_DATE = "end_date";
    static final String JSON_COMPLETED = "completed";
    
    static final String JSON_ITEMS = "items";
//    double x=810;
//    double y=810;
    double x=0;
    double y=0;
    int color=100;
    DataManager dataManagerLoad;
        public double getX(){return x;}
        public double getY(){return y;}
        public void setX(double x){this.x=x;}
        public void sety(double y){this.y=y;}
    /**
     * 
     * @param data
     * @param regionName
     * @param filePath
     * @param Directory
     * @throws IOException 
     */    
    
    @Override
    public void newData(AppDataComponent data, String regionName, String filePath, String exportDirectory) throws IOException {
            DataManager dataManager = (DataManager)data;
//            Pane mapPane = dataManager.getMapPane();
            StackPane mapPane = dataManager.getLeftp();
            String polygons = FileUtils.readFileToString(new File(filePath));
            CountryJSON countryJson = JSON.parseObject(polygons, CountryJSON.class) ;
            
            
            dataManager.reset(); 
                        
    // LOAD ALL THE DATA INTO THE WORKSPACE
    //		app.getWorkspaceComponent().reloadWorkspace();
    
     color=100;
            dataManager.addBar();
        
            DesignMap designMap =new DesignMap(regionName,exportDirectory+"/"+regionName,filePath);
                
            dataManager.setDesignMap(designMap);
            
        
            dataManager.setCountryJSON(countryJson);
            
            //load map to UI
            int numOfRegions=countryJson.getNUMBER_OF_SUBREGIONS();
            ArrayList<SubRegions> subRegions =countryJson.getSUBREGIONS();
            
            
            cauculateMapRate(subRegions, numOfRegions);
            Group gp = loadSubRegions(subRegions, numOfRegions);
            gp.rotateProperty().set(-90);
            
           
            //progress bar
            Timeline tl1= new Timeline(new KeyFrame(
         Duration.millis(400),
                ae->{
                    dataManager.setP(0.2);
                }));
         tl1.play();
         Timeline tl3= new Timeline(new KeyFrame(
         Duration.millis(600),
                ae->{
                    dataManager.setP(0.3);
                }));
            tl3.play();
        Timeline tl2= new Timeline(new KeyFrame(
         Duration.millis(800),
                ae->{
                    dataManager.setP(0.4);
                }));
            tl2.play();
         Timeline tl5= new Timeline(new KeyFrame(
         Duration.millis(1000),
                ae->{
                    dataManager.setP(0.5);
                }));
            tl5.play();
            
            //load map to UI
         Timeline tl6= new Timeline(new KeyFrame(
         Duration.millis(1200),
                ae->{
                    dataManager.setP(0.6);
                }));
            tl6.play();
         Timeline tl7= new Timeline(new KeyFrame(
         Duration.millis(1400),
                ae->{
                    dataManager.setP(0.7);
                }));
            tl7.play();
        Timeline tl8= new Timeline(new KeyFrame(
         Duration.millis(1600),
                ae->{
                    dataManager.setP(0.8);
                }));
            tl8.play();
        Timeline tl9= new Timeline(new KeyFrame(
         Duration.millis(1800),
                ae->{
                    dataManager.setP(0.9);
                }));
            tl9.play();
            
        
        Timeline tl11= new Timeline(new KeyFrame(
         Duration.millis(2000),
                ae->{
                    dataManager.setP(1);
                    
            //set up the color of every subReigon
            ObservableList<Node> group =  gp.getChildren();
            for(Node node: group){
                Polygon plg = (Polygon)node;
             plg.setFill(Color.rgb(color,color,color));
             color=color+5;
            }
            
        // Add Data To Table
                    ObservableList<SubRegionForTable> obl = dataManager.getSubRegionForTable();
                     //reset color = 100;save to the DesignMap ,
                    color=100;
                    for (int i = 1; i <= countryJson.getNUMBER_OF_SUBREGIONS(); i++) {
                        //Add Data To Table
                        SubRegionForTable s = new SubRegionForTable ();
                        s.setNameForTable("Name "+i);
                        s.setCapitalForTable("Capital "+i);
                        s.setLeaderForTable("Leader "+i);
                        s.setRGB(color, color, color);
                        //Add Data for saving file
                        obl.add(s);
                        SubRegion sbr = new SubRegion();
                        sbr.setName(s.getNameForTable());
                        sbr.setCapital(s.getCapitalForTable());
                        sbr.setLeader(s.getLeaderForTable());
                        sbr.setRGB(color, color, color);
                        dataManager.getDesignMap().getTableList().add(sbr);
                        color= color +5;
                    }
                    
         //new file
            File file = new File(PATH_WORK+regionName+".rvme");
            if(!file.exists()){
                        try {
                            file.createNewFile();
                            FileUtils.writeStringToFile(file, JSON.toJSONString(dataManager.getDesignMap()));
                        } catch (IOException ex) {
                            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
                        }
            }
        //Add Data To Map
//                    System.out.println(mapPane);
//                 ((BorderPane)(mapPane.getChildren().get(0))).centerProperty().set(gp);
//            mapPane.centerProperty().set(gp);
            mapPane.getChildren().add(gp);
                    dataManager.removeBar();
                    
                    Binder bd = new Binder(gp, dataManager.getApp());
                }));
        tl11.play();
    }        
        
        
        
        
        
        
        
        
      
        
        
    /**
     * This method is for saving user work.
     * 
     * @param data The data management component for this application.
     * 
     * @param filePath Path (including file name/extension) to where
     * to save the data to.
     * 
     * @throws IOException Thrown should there be an error writing 
     * out data to the file.
     */
    public void saveData(AppDataComponent data, String filePath) throws IOException {
    DataManager dataManager = (DataManager)data;
    
     ObservableList<SubRegionForTable> obl = dataManager.getSubRegionForTable();
             ArrayList<SubRegion> tableList = dataManager.getDesignMap().getTableList();
             tableList.clear();
             for( SubRegionForTable sbr : obl){
                 SubRegion sr =  new SubRegion();
                 //load data from file
                sr.setName(sbr.getNameForTable());
                sr.setCapital(sbr.getCapitalForTable());
                sr.setLeader(sbr.getLeaderForTable());
                sr.setFlagImagePath("C:\\Users\\admin\\Desktop\\Stony Brook\\CSE 219\\Summer HomeWork\\RegioVincoMapEditor\\export\\The World\\The World.png");
                //set data to the table
                
                tableList.add(sr);
            }
    
    File file =  new File(PATH_WORK+filePath);
    file.createNewFile();
        FileUtils.writeStringToFile(file, JSON.toJSONString(dataManager.getDesignMap()));
    }
    @Override
    public void saveData(AppDataComponent data, File file) throws IOException {
//	 GET THE DATA
	DataManager dataManager = (DataManager)data;
        
        String st = file.getAbsolutePath();
//        File oldFile = new File(dataManager.getDesignMap().getParentDirectory()+"/"+dataManager.getDesignMap().getRegionName()+".rvme");
        if(file.exists()){
        file.delete();
        }
        File newFile=new File(st);
        newFile.createNewFile();
        
         ObservableList<SubRegionForTable> obl = dataManager.getSubRegionForTable();
             ArrayList<SubRegion> tableList = dataManager.getDesignMap().getTableList();
             tableList.clear();
             for( SubRegionForTable sbr : obl){
                 SubRegion sr =  new SubRegion();
                 //load data from file
                sr.setName(sbr.getNameForTable());
                sr.setCapital(sbr.getCapitalForTable());
                sr.setLeader(sbr.getLeaderForTable());
                sr.setRGB(sbr.getRed(), sbr.getGreen(), sbr.getBlue());
                //set data to the table
                
                tableList.add(sr);
            }
        FileUtils.writeStringToFile(newFile, JSON.toJSONString(dataManager.getDesignMap()));
    }
    
    /**
     * This method loads data from a JSON formatted file into the data 
     * management component and then forces the updating of the workspace
     * such that the user may edit the data.
     * 
     * @param path
     * @param data Data management component where we'll load the file into.
     * 
     * @param filePath Path (including file name/extension) to where
     * to load the data from.
     * @return 
     * 
     * @throws IOException Thrown should there be an error reading
     * in data from the file.
     */
    
    public DesignMap loadFile(String path) throws IOException{
        String   data2 = FileUtils.readFileToString(new File(path));
        DesignMap designMap = JSON.parseObject(data2, DesignMap.class);
        dataManagerLoad = new DataManager();
       dataManagerLoad.setDesignMap(designMap);
        return designMap;
    }
    
    /*
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
//	 CLEAR THE OLD DATA OUT
        DataManager dataManager = (DataManager)data;
        Workspace workspace = (Workspace) dataManager.getApp().getWorkspaceComponent();
        workspace.reloadWorkspace();
        System.out.println("1");
        dataManager.getMapPane().getStylesheets().clear();
//        x = (dataManager.getGui().getAppPane().getWidth())/2;
//         y = dataManager.getGui().getAppPane().getHeight()-((Pane)dataManager.getGui().getAppPane().getTop()).getHeight()-((VBox)(dataManager.getApp().getWorkspaceComponent().getWorkspace().getChildren().get(0))).getWidth();
//        System.out.println(x);
//        System.out.println(y);
            DesignMap designMap =loadFile(filePath);
            String polygons = FileUtils.readFileToString(new File(designMap.getPolygonsPath()));
            CountryJSON countryJson = JSON.parseObject(polygons, CountryJSON.class);
        
       
            dataManager.addBar();
//            Pane mapPane = dataManager.getMapPane();
            StackPane mapPane = dataManager.getLeftp();
            
            //reset data
            dataManager.reset();
            dataManager.setDesignMap(designMap);
            dataManager.setCountryJSON(countryJson);
            
            //load map to UI
            int numOfRegions=countryJson.getNUMBER_OF_SUBREGIONS();
            ArrayList<SubRegions> subRegions =countryJson.getSUBREGIONS();
            
            cauculateMapRate(subRegions, numOfRegions);
            Group gp = loadSubRegions(subRegions, numOfRegions);
            
            gp.rotateProperty().set(-90);
            
            
            Timeline tl1= new Timeline(new KeyFrame(
         Duration.millis(400),
                ae->{
                    dataManager.setP(0.2);
                }));
         tl1.play();
         Timeline tl3= new Timeline(new KeyFrame(
         Duration.millis(600),
                ae->{
                    dataManager.setP(0.3);
                }));
            tl3.play();
        Timeline tl2= new Timeline(new KeyFrame(
         Duration.millis(800),
                ae->{
                    dataManager.setP(0.4);
                }));
            tl2.play();
         Timeline tl5= new Timeline(new KeyFrame(
         Duration.millis(1000),
                ae->{
                    dataManager.setP(0.5);
                }));
            tl5.play();
            
            //load map to UI
         Timeline tl6= new Timeline(new KeyFrame(
         Duration.millis(1200),
                ae->{
                    dataManager.setP(0.6);
                }));
            tl6.play();
         Timeline tl7= new Timeline(new KeyFrame(
         Duration.millis(1400),
                ae->{
                    dataManager.setP(0.7);
                }));
            tl7.play();
        Timeline tl8= new Timeline(new KeyFrame(
         Duration.millis(1600),
                ae->{
                    dataManager.setP(0.8);
                }));
            tl8.play();
        Timeline tl9= new Timeline(new KeyFrame(
         Duration.millis(1800),
                ae->{
                    dataManager.setP(0.9);
                }));
            tl9.play();
            
        
        Timeline tl11= new Timeline(new KeyFrame(
         Duration.millis(2000),
                ae->{
                    dataManager.setP(1);
                    ObservableList<SubRegionForTable> obl = dataManager.getSubRegionForTable();
             ArrayList<SubRegion> tableList = designMap.getTableList();
             for(SubRegion s : tableList){
                 SubRegionForTable sbr = new SubRegionForTable();
                 //load data from file
                sbr.setNameForTable(s.getName());
                sbr.setCapitalForTable(s.getCapital());
                sbr.setLeaderForTable(s.getLeader());
                sbr.setRGB(s.getRed(),s.getGreen(),s.getBlue());
                //set data to the table
                obl.add(sbr);
                
                //load  data to the Map
          }
             //set up background Color
                int bgred = designMap.getBackgroundRed();
                int bggreen =designMap.getBackgroundGreen();
                int bgblue = designMap.getBackgroundBlue();
                    System.out.println(bgred+","+bggreen+","+bgblue);
                Color bgcolor= Color.rgb(bgred,bggreen, bgblue);
                   
                int bdred = designMap.getBorderRed();
                int bdgreen =designMap.getBorderGreen();
                int bdblue = designMap.getBorderBlue();
                Color bdcolor= Color.rgb(bdred, bdgreen, bdblue);
                dataManager.getMapPane().setBackground(new Background(new BackgroundFill(bgcolor,null,null)));
                
                //table
              ObservableList<Node> group =  gp.getChildren();
             for(int i=0;i<group.size();i++){
                 Polygon plg = (Polygon)group.get(i);
                 if(i<tableList.size()){
                     //set up subRegion Color
                 plg.setFill(Color.rgb(tableList.get(i).getRed(),tableList.get(i).getGreen(),tableList.get(i).getBlue()));
                    //set up border Color
                 plg.setStroke(Color.rgb(designMap.getBorderRed(), designMap.getBorderGreen(), designMap.getBorderBlue()));   
                    //set up Thickness
                 plg.setStrokeWidth(designMap.getBorderThickness());
                 }
             }
             
//             workspace.setupWorkSpace( bgcolor, bdcolor, designMap.getZoomLevel(), designMap.getBorderThickness());
             //set up zoomLevel
             gp.setScaleX(designMap.getZoomLevel());
             gp.setScaleY(designMap.getZoomLevel());
             
            //add to the mapPane
//                    mapPane.centerProperty().set(gp);
//                    ((BorderPane)(mapPane.getChildren().get(0))).centerProperty().set(gp);
//                    mapPane.centerProperty().set(gp);
//            ((StackPane)mapPane.getChildren().get(0)).getChildren().add(gp);   
System.out.println("5");
                mapPane.getChildren().add(gp);
                System.out.println("6");
                    dataManager.removeBar();
                    Binder bd = new Binder(gp, dataManager.getApp());
                }));
        tl11.play();
            
    }
*/public void loadData(AppDataComponent data, String filePath) throws IOException {
//	 CLEAR THE OLD DATA OUT
        DataManager dataManager = (DataManager)data;
        Workspace workspace = (Workspace) dataManager.getApp().getWorkspaceComponent();
        workspace.reloadWorkspace();
        dataManager.getMapPane().getStylesheets().clear();
//        x = (dataManager.getGui().getAppPane().getWidth())/2;
//         y = dataManager.getGui().getAppPane().getHeight()-((Pane)dataManager.getGui().getAppPane().getTop()).getHeight()-((VBox)(dataManager.getApp().getWorkspaceComponent().getWorkspace().getChildren().get(0))).getWidth();
//        System.out.println(x);
//        System.out.println(y);
            DesignMap designMap =loadFile(filePath);
            String polygons = FileUtils.readFileToString(new File(designMap.getPolygonsPath()));
            CountryJSON countryJson = JSON.parseObject(polygons, CountryJSON.class);
        
       
            dataManager.addBar();
            StackPane mapPane = dataManager.getLeftp();
        
            //reset data
            dataManager.reset();
            dataManager.setDesignMap(designMap);
            dataManager.setCountryJSON(countryJson);
            
            //load map to UI
            int numOfRegions=countryJson.getNUMBER_OF_SUBREGIONS();
            ArrayList<SubRegions> subRegions =countryJson.getSUBREGIONS();
            
            cauculateMapRate(subRegions, numOfRegions);
            Group gp = loadSubRegions(subRegions, numOfRegions);
            
            gp.rotateProperty().set(-90);
            
            
            Timeline tl1= new Timeline(new KeyFrame(
         Duration.millis(400),
                ae->{
                    dataManager.setP(0.2);
                }));
         tl1.play();
         Timeline tl3= new Timeline(new KeyFrame(
         Duration.millis(600),
                ae->{
                    dataManager.setP(0.3);
                }));
            tl3.play();
        Timeline tl2= new Timeline(new KeyFrame(
         Duration.millis(800),
                ae->{
                    dataManager.setP(0.4);
                }));
            tl2.play();
         Timeline tl5= new Timeline(new KeyFrame(
         Duration.millis(1000),
                ae->{
                    dataManager.setP(0.5);
                }));
            tl5.play();
            
            //load map to UI
         Timeline tl6= new Timeline(new KeyFrame(
         Duration.millis(1200),
                ae->{
                    dataManager.setP(0.6);
                }));
            tl6.play();
         Timeline tl7= new Timeline(new KeyFrame(
         Duration.millis(1400),
                ae->{
                    dataManager.setP(0.7);
                }));
            tl7.play();
        Timeline tl8= new Timeline(new KeyFrame(
         Duration.millis(1600),
                ae->{
                    dataManager.setP(0.8);
                }));
            tl8.play();
        Timeline tl9= new Timeline(new KeyFrame(
         Duration.millis(1800),
                ae->{
                    dataManager.setP(0.9);
                }));
            tl9.play();
            
        
        Timeline tl11= new Timeline(new KeyFrame(
         Duration.millis(2000),
                ae->{
                    dataManager.setP(1);
                    ObservableList<SubRegionForTable> obl = dataManager.getSubRegionForTable();
             ArrayList<SubRegion> tableList = designMap.getTableList();
             for(SubRegion s : tableList){
                 SubRegionForTable sbr = new SubRegionForTable();
                 //load data from file
                sbr.setNameForTable(s.getName());
                sbr.setCapitalForTable(s.getCapital());
                sbr.setLeaderForTable(s.getLeader());
                sbr.setRGB(s.getRed(),s.getGreen(),s.getBlue());
                //set data to the table
                obl.add(sbr);
                
                //load  data to the Map
          }
             //set up background Color
                int bgred = designMap.getBackgroundRed();
                int bggreen =designMap.getBackgroundGreen();
                int bgblue = designMap.getBackgroundBlue();
                    System.out.println(bgred+","+bggreen+","+bgblue);
                Color bgcolor= Color.rgb((int)(bgred/255), (int)(bggreen/255), (int)(bgblue/255));
                   
                int bdred = designMap.getBorderRed();
                int bdgreen =designMap.getBorderGreen();
                int bdblue = designMap.getBorderBlue();
                Color bdcolor= Color.rgb(bdred, bdgreen, bdblue);
                dataManager.getMapPane().setBackground(new Background(new BackgroundFill(bgcolor,null,null)));
             
                //table
              ObservableList<Node> group =  gp.getChildren();
             for(int i=0;i<group.size();i++){
                 Polygon plg = (Polygon)group.get(i);
                 if(i<tableList.size()){
                     //set up subRegion Color
                 plg.setFill(Color.rgb(tableList.get(i).getRed(),tableList.get(i).getGreen(),tableList.get(i).getBlue()));
                    //set up border Color
                 plg.setStroke(Color.rgb(designMap.getBorderRed(), designMap.getBorderGreen(), designMap.getBorderBlue()));   
                    //set up Thickness
                 plg.setStrokeWidth(designMap.getBorderThickness());
                 }
             }
             
             workspace.setupWorkSpace( bgcolor, bdcolor, designMap.getZoomLevel(), designMap.getBorderThickness());
             //set up zoomLevel
             gp.setScaleX(designMap.getZoomLevel());
             gp.setScaleY(designMap.getZoomLevel());
             
            //add to the mapPane
//                    mapPane.centerProperty().set(gp);
mapPane.getChildren().add(gp);
                    dataManager.removeBar();
                    
                }));
        tl11.play();
        
        
        
        ((Workspace)dataManager.getApp().getWorkspaceComponent()).getTable().setRowFactory(r -> {
            TableRow row = new TableRow<>();
            //foolproof design
            row.setOnMouseClicked(clicked->{
            //toDoListController.processDisableButton();
                int index = ((Workspace)dataManager.getApp().getWorkspaceComponent()).getTable().getSelectionModel().getSelectedIndex();
                for (Node plg: gp.getChildren()){
                    ((Polygon)plg).setEffect(null);
                }
                Shadow highlight = new Shadow(1, Color.GREEN);
                gp.getChildren().get(index).setEffect(highlight);
                if (clicked.getClickCount() ==2&& !row.isEmpty()) {  
                    SubRegionDialog sbd=  new SubRegionDialog(dataManager.getApp());
                    ((Workspace)dataManager.getApp().getWorkspaceComponent()).setSBD(sbd);
                
                    ((Workspace)dataManager.getApp().getWorkspaceComponent()).getController().processEditItem(sbd);
                
                }
            });
            return row;
        });
            
        
            ((Workspace)dataManager.getApp().getWorkspaceComponent()).getNextBt().setOnAction(e->{
            ((Workspace)dataManager.getApp().getWorkspaceComponent()).getTable().getSelectionModel().selectNext();
            SubRegionForTable subRegionForTable=  ((Workspace)dataManager.getApp().getWorkspaceComponent()).getTable().getSelectionModel().getSelectedItem();
                ((Workspace)dataManager.getApp().getWorkspaceComponent()).getSBD().setRegionName(subRegionForTable.getNameForTable());
                ((Workspace)dataManager.getApp().getWorkspaceComponent()).getSBD().setCapital(subRegionForTable.getCapitalForTable());
                ((Workspace)dataManager.getApp().getWorkspaceComponent()).getSBD().setLeaderName(subRegionForTable.getLeaderForTable());
            int index = ((Workspace)dataManager.getApp().getWorkspaceComponent()).getTable().getSelectionModel().getSelectedIndex();
                for (Node plg: gp.getChildren()){
                    ((Polygon)plg).setEffect(null);
                }
                Shadow highlight = new Shadow(1, Color.GREEN);
                gp.getChildren().get(index).setEffect(highlight);
                System.out.println("asdf");
            });
            ((Workspace)dataManager.getApp().getWorkspaceComponent()).getPreBt().setOnAction(e->{
            ((Workspace)dataManager.getApp().getWorkspaceComponent()).getTable().getSelectionModel().selectPrevious();
            System.out.println("asdfasdff");
            SubRegionForTable subRegionForTable=  ((Workspace)dataManager.getApp().getWorkspaceComponent()).getTable().getSelectionModel().getSelectedItem();
                ((Workspace)dataManager.getApp().getWorkspaceComponent()).getSBD().setRegionName(subRegionForTable.getNameForTable());
                ((Workspace)dataManager.getApp().getWorkspaceComponent()).getSBD().setCapital(subRegionForTable.getCapitalForTable());
                ((Workspace)dataManager.getApp().getWorkspaceComponent()).getSBD().setLeaderName(subRegionForTable.getLeaderForTable());
            int index = ((Workspace)dataManager.getApp().getWorkspaceComponent()).getTable().getSelectionModel().getSelectedIndex();
                for (Node plg: gp.getChildren()){
                    ((Polygon)plg).setEffect(null);
                }
                Shadow highlight = new Shadow(1, Color.GREEN);
                gp.getChildren().get(index).setEffect(highlight);
            });
                
//        dataManager.setGp(gp);
        Binder bd = new Binder(gp, dataManager.getApp());
            
    }
    public double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    public int getDataAsInt(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber)value;
        return number.bigIntegerValue().intValue();
    }
    
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    private Group loadSubRegions(ArrayList<SubRegions> subRegions, int num){
        Group gp = new Group();
        for (int i = 0; i < num; i++){
            int numOfPolygons =subRegions.get(i).getNUMBER_OF_SUBREGION_POLYGONS();
            for (int j = 0; j < numOfPolygons; j++){
                ArrayList<rvme.data.Polygon> polygon= subRegions.get(i).getSUBREGION_POLYGONS().get(j);
                gp.getChildren().add(createPolygon(polygon)); 
            }
        }
        return gp;
    }
    
    private Polygon createPolygon(ArrayList<rvme.data.Polygon> polygon){
//        color=color+5;
        Polygon plg = new Polygon();
        for (int i = 0; i < polygon.size(); i++){
            plg.getPoints().add(500/y*0.65*polygon.get(i).getY());
            plg.getPoints().add(800/x*0.65*polygon.get(i).getX());
        }
//        plg.setFill(Color.rgb(color,color,color));
        plg.setStroke(Color.BLACK);
        plg.setStrokeWidth(0.5);
        
        return plg;
    }
    
    private void cauculateMapRate(ArrayList<SubRegions>  subRegions,int num){
        double maxX=subRegions.get(0).getSUBREGION_POLYGONS().get(0).get(0).getX();
        double maxY=subRegions.get(0).getSUBREGION_POLYGONS().get(0).get(0).getY();
        double minX=subRegions.get(0).getSUBREGION_POLYGONS().get(0).get(0).getX();
        double minY=subRegions.get(0).getSUBREGION_POLYGONS().get(0).get(0).getY();
        
        for (int i = 0; i < num; i++){
            int numOfPolygons =subRegions.get(i).getNUMBER_OF_SUBREGION_POLYGONS();
            for (int j = 0; j < numOfPolygons; j++){
                ArrayList<rvme.data.Polygon> polygon= subRegions.get(i).getSUBREGION_POLYGONS().get(j);
                Polygon plg = new Polygon();
                    for (int k = 0; k < polygon.size(); k++){
                        //getMinX
                        if(minX >=  polygon.get(k).getX()){
                               minX=polygon.get(k).getX();
                        }
                        //getMinY
                        if(minY >= polygon.get(k).getY()){
                            minY=polygon.get(k).getY();
                        }
                        //getMaxX
                        if(maxX <=  polygon.get(k).getX()){
                            maxX =  polygon.get(k).getX();
                        }
                        //getMaxY
                        if(maxY <= polygon.get(k).getY()){
                          maxY = polygon.get(k).getY();
                        }
                    }
            }
        }
        x=maxX-minX;
        y=maxY-minY;
//        System.out.println(maxX-minX);
//        System.out.println(maxY-minY);
    }
    /**
     * This method would be used to export data to another format,
     * which we're not doing in this assignment.
     */
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
            // GET THE DATA
	DataManager dataManager = (DataManager)data;
        DesignMap designMap = dataManager.getDesignMap();
        
        World world =  new World();
        world.setName(dataManager.getDesignMap().getRegionName());
        ArrayList<subRegionForWorld> list =  new ArrayList();
        ArrayList<SubRegion> list2 = designMap.getTableList();
        
     //has_capitals
        boolean has_capitals=true;
        for(SubRegion checkCapital :list2){
              if (checkCapital.getCapital()==null){
                has_capitals=false;
            }
        }
        world.setSubregions_have_capitals(has_capitals);
        
//        //  has_flag
        boolean has_flag=true;
        for(SubRegion checkFlag :list2){
            String s=checkFlag.getFlagImagePath();
            if(s==null){ 
                has_flag=false;
                break;
            }
           else{
                File file =  new File(checkFlag.getFlagImagePath());
                  if (!file.exists()){
                    has_flag=false;
                    break;
                }
           }
        }
        world.setSubregions_have_flags(has_flag);
        
//        //has_leader
        boolean has_leader=true;
        for(SubRegion checkLeader :list2){
             String s=checkLeader.getFlagImagePath();
             if(s==null){
                 has_leader=false;
                 break;
             }
             else{
                 File file =  new File(checkLeader.getLeaderImagePath());
              if (checkLeader.getLeader()==null||!(file.exists())){
                  has_leader=false;
                  break;
                }
           }
        }
        world.setSubregions_have_leaders(has_leader);
        
        
        for(SubRegion s :list2){
            subRegionForWorld sfw =  new subRegionForWorld();
            sfw.setName(s.getName());
            sfw.setCapital(s.getCapital());
            sfw.setLeader(s.getLeader());
            sfw.setBlue(s.getBlue());
            sfw.setRed(s.getRed());
            sfw.setGreen(s.getGreen());
            list.add(sfw);
        }
        world.setSubregions(list);
        
        //create parentDirectory
         String parentDirectory=designMap.getParentDirectory();
            File dir= new File(parentDirectory);
            
            if(!(dir.exists())){
                dir.mkdir();
            }
        
        String rvmFile= parentDirectory+"/"+designMap.getRegionName()+".rvm";
         
        File file = new File(rvmFile);
        file.createNewFile();
        FileUtils.writeStringToFile(file, JSON.toJSONString(world));
        
        if(designMap.getWidth()==0||designMap.getHeight()==0){
          designMap.setWidth(802);
          designMap.setHeight(536);
        }
        //snapshot
            WritableImage wim = new WritableImage((int)designMap.getWidth(), (int)designMap.getHeight());
            WritableImage snapImage = dataManager.getLeftp().snapshot(new SnapshotParameters(), wim);
            try{
//                File parentD = new File(dataManager.getDesignMap().getParentDirectory()+"/"+dataManager.getDesignMap().getRegionName());
//                if(!parentD.exists()){
//                    parentD.mkdir();
//                }
//                
                File savePng= new File(designMap.getParentDirectory()+"/"+designMap.getRegionName()+".png");
                if(!(savePng.exists())){
                savePng.createNewFile();
                }
                
                
            if (file != null) 
                
                try {
                    ImageIO.write(SwingFXUtils.fromFXImage(wim, null), "png", savePng);
                } catch (Exception s) {
                }
            }catch(Exception ex){
                }
            
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Your DesignMap Is Exported!");

            alert.showAndWait();
    }
    
    /**
     * This method is provided to satisfy the compiler, but it
     * is not used by this application.
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        
    }

    

    
}
