/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvme.test_bed;

import com.alibaba.fastjson.JSON;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.FileUtils;
import rvme.data.DataManager;
import rvme.data.DesignMap;
import rvme.data.SubRegion;
import rvme.data.World;
import rvme.data.subRegionForWorld;
import rvme.file.FileManager;

/**
 *
 * @author admin
 */
public class TestSave{
    public static DataManager dataManagerAndor = new DataManager();
    public static DataManager dataManagerSan = new DataManager();
    public static DataManager dataManagerSlo = new DataManager();
    public static void main(String[] args) {
//		createAndorra();
//                createSanMarino();
//                createSlovakia();
String s1 = "Hello";
String s2 = "Hello";
if (s1 == s2) System.out.println(1);

String s3 = new String("Hello");
if (s1 == s3) System.out.println(2);

String s4 = "Hell";
s4 += "o";
if (s1 == s4) System.out.println(3);

	}

    public static DesignMap createAndorra(){
        
        DesignMap designMap = new DesignMap();
           ArrayList<SubRegion> subRegionListAndorra = new ArrayList();
           try {
            //hard code  from prof's file
                String data = FileUtils.readFileToString(new File("./export/The World/Europe/Andorra/Andorra.rvm"));
                World world = JSON.parseObject(data, World.class);
//                System.out.println(world.toString());
                List<subRegionForWorld> al = world.getSubregions();
                for(subRegionForWorld sf :al){
                    SubRegion sb = new SubRegion(sf.getName(),sf.getCapital(),sf.getLeader(),"./export/The World/Europe/Andorra/"+sf.getLeader()+".png","./export/The World/Europe/Andorra/"+sf.getName()+" Flag.png",sf.getRed(),sf.getGreen(),sf.getBlue());
                    subRegionListAndorra.add(sb);
                }
      //hard code by hand
  SubRegion sb = new SubRegion("TestName","TestCapital","TestLeader","./export/The World/Europe/Andorra/TestLeader Flag.png","./export/The World/Europe/Andorra/TestLeader Flag.png",180,180,180);
  subRegionListAndorra.add(sb);
            designMap.setTableList(subRegionListAndorra);
            
            //For Test Changing value
//            ((SubRegion)(designMap.getTableList().get(0))).setName("HelloCountry");
//            ((SubRegion)(designMap.getTableList().get(0))).setCapital("HelloCity");
//             ((SubRegion)(designMap.getTableList().get(0))).setLeader("HelloLeader");
             
            designMap.setSubregions_have_capitals(world.isSubregions_have_capitals());
            designMap.setSubregions_have_flags(world.isSubregions_have_flags());
            designMap.setSubregions_have_leaders(world.isSubregions_have_leaders());
            designMap.setRegionName("Andorra");
            designMap.setBackgroundColor(1, 1, 1);
            designMap.setBorderColor(100, 100, 100);
            designMap.setHeight(100);
            designMap.setWidth(100);
            designMap.setZoomLevel(1.0);
            designMap.setBorderThickness(5.0);
            designMap.setPolygonsPath("./raw_map_data/Andorra.json");
            designMap.setAnthemPath("./export/The World/Europe/Andorra/"+"Andorra National Anthem.mid");
            designMap.setParentDirectory("./export/The World/Europe");
            designMap.setFlagImagePath("./work/World/Europe/Andorra/Andorra.png");
            designMap.setLeaderImagePath("./work/World/Europe/Andorra/AndorraLeader.png");
            designMap.getFlagImageLocation().add(1);
            designMap.getFlagImageLocation().add(1);
            designMap.getLeaderImageLocation().add(2);
            designMap.getLeaderImageLocation().add(2);
            designMap.getScrollMapLocation().add(3);
            designMap.getScrollMapLocation().add(3);
            designMap.setBorderThickness(5.0);
                
            // i/o
            dataManagerAndor.setDesignMap(designMap);
            FileManager fileManager= new FileManager();
            fileManager.saveData(dataManagerAndor, "Andorra.rvme");
                
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
            return designMap;
    
    }
    
    public static DesignMap createSanMarino(){
        //hard code
        DesignMap designMap = new DesignMap();
        ArrayList<SubRegion> subRegionListSanMarino = new ArrayList();
        try {
            //hard code  for creating data
                String data = FileUtils.readFileToString(new File("./export/The World/Europe/San Marino/San Marino.rvm"));
                World world = JSON.parseObject(data, World.class);
                
                List<subRegionForWorld> al = world.getSubregions();
                for(subRegionForWorld sf :al){
                    SubRegion sb = new SubRegion(sf.getName(),sf.getCapital(),sf.getLeader(),"./export/The World/Europe/San Marino/"+sf.getLeader()+".png","./export/The World/Europe/San Marino/"+sf.getName()+" Flag.png",sf.getRed(),sf.getGreen(),sf.getBlue());
                    subRegionListSanMarino.add(sb);
                }
                SubRegion sb = new SubRegion("TestName1","TestCapital1","TestLeader1","./export/The World/Europe/Andorra/TestName1.png","./export/The World/Europe/Andorra/TestLeader1 Flag.png",180,180,180);
//            subRegionListSanMarino.add(sb);
//            ((SubRegion)(designMap.getTableList().get(0))).setName("MarinoCountry");
//            ((SubRegion)(designMap.getTableList().get(0))).setCapital("MarinoCapital");
//            ((SubRegion)(designMap.getTableList().get(0))).setLeader("MarinoLeader");
            
            designMap.setSubregions_have_capitals(world.isSubregions_have_capitals());
            designMap.setSubregions_have_flags(world.isSubregions_have_flags());
            designMap.setSubregions_have_leaders(world.isSubregions_have_leaders());
            designMap.setTableList(subRegionListSanMarino);
            designMap.setBackgroundColor(200, 200, 200);
            designMap.setBorderColor(200, 200, 200);
            designMap.setHeight(200);
            designMap.setWidth(200);
            designMap.setRegionName("San Marino");
            designMap.setZoomLevel(2.0);
            designMap.setPolygonsPath("./raw_map_data/San Marino.json");
            designMap.setAnthemPath("./export/The World/Europe/San Marino/"+"San Marino National Anthem.mid");
            designMap.setParentDirectory("./export/The World/Europe");
            
            designMap.setFlagImagePath("./work/World/Europe/Andorra/San Marino.png");
            designMap.setLeaderImagePath("./work/World/Europe/Andorra/San MarinoLeader.png");
            designMap.getFlagImageLocation().add(1);
            designMap.getFlagImageLocation().add(1);
            designMap.getLeaderImageLocation().add(2);
            designMap.getLeaderImageLocation().add(2);
            designMap.getScrollMapLocation().add(3);
            designMap.getScrollMapLocation().add(3);
            designMap.setBorderThickness(5.0);
            
            
            
            
            
            // i/o
            dataManagerSan.setDesignMap(designMap);
            FileManager fileManager= new FileManager();
            fileManager.saveData(dataManagerSan, "San Marino.rvme");
                
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    
            return designMap;
    }
    
    public static DesignMap createSlovakia(){
        DesignMap designMap = new DesignMap();
    ArrayList<SubRegion> subRegionListSlovakia = new ArrayList();
        //hard code
        try {
            //hard code  for creating data
                String data = FileUtils.readFileToString(new File("./export/The World/Europe/Slovakia/Slovakia.rvm"));
                World world = JSON.parseObject(data, World.class);
//                System.out.println(world.toString());
                List<subRegionForWorld> al = world.getSubregions();
                for(subRegionForWorld sf :al){
                    SubRegion sb = new SubRegion(sf.getName(),sf.getCapital(),sf.getLeader(),"./export/The World/Europe/Slovakia/"+sf.getLeader()+".png","./export/The World/Europe/Slovakia/"+sf.getName()+" Flag.png",sf.getRed(),sf.getGreen(),sf.getBlue());
                    subRegionListSlovakia.add(sb);
                }
                 SubRegion sb = new SubRegion("TestName2","TestCapital2","TestLeader2","./export/The World/Europe/Andorra/TestName2.png","./export/The World/Europe/Andorra/TestLeader2 Flag.png",180,180,180);
//            subRegionListSlovakia.add(sb);
//            ((SubRegion)(designMap.getTableList().get(0))).setName("SlovakiaCountry");
//            ((SubRegion)(designMap.getTableList().get(0))).setCapital("SlovakiaCapital");
//             ((SubRegion)(designMap.getTableList().get(0))).setLeader("SlovakiaLeader");
            designMap.setSubregions_have_capitals(world.isSubregions_have_capitals());
            designMap.setSubregions_have_flags(world.isSubregions_have_flags());
            designMap.setSubregions_have_leaders(world.isSubregions_have_leaders());
            designMap.setTableList(subRegionListSlovakia);
            designMap.setBackgroundColor(50, 50, 50);
            designMap.setBorderColor(50, 50, 50);
            designMap.setHeight(50);
            designMap.setWidth(50);
            designMap.setRegionName("Slovakia");
            designMap.setZoomLevel(3.0);
            designMap.setPolygonsPath("./raw_map_data/Slovakia.json");
            designMap.setAnthemPath("./export/The World/Europe/Slovakia/"+"Slovakia National Anthem.mid");
            designMap.setParentDirectory("./export/The World/Europe");
            designMap.setFlagImagePath("./work/World/Europe/Andorra/Slovakia.png");
            designMap.setLeaderImagePath("./work/World/Europe/Andorra/SlovakiaLeader.png");
            designMap.getFlagImageLocation().add(1);
            designMap.getFlagImageLocation().add(1);
            designMap.getLeaderImageLocation().add(2);
            designMap.getLeaderImageLocation().add(2);
            designMap.getScrollMapLocation().add(3);
            designMap.getScrollMapLocation().add(3);
            designMap.setBorderThickness(5.0);
            
            
            // i/o
            dataManagerSlo.setDesignMap(designMap);
            FileManager fileManager= new FileManager();
            fileManager.saveData(dataManagerSlo, "Slovakia.rvme");
                
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return designMap;
    }
    
    
    
    
    
    
    
    
    
    
}
