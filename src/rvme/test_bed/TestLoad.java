/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvme.test_bed;

import java.io.IOException;
import java.util.ArrayList;
import rvme.data.DataManager;
import rvme.data.DesignMap;
import rvme.data.SubRegion;
import rvme.file.FileManager;

/**
 *
 * @author admin
 */
public class TestLoad{
    
    public static void main(String[] args) {
		loadAndorraMap();
                loadSanMarinoMap();
                loadSlovakiaMap();
	}
    
    public static DesignMap loadAndorraMap(){
         DataManager dataManager = new TestSave().dataManagerAndor;
         DesignMap designMap = new DesignMap();
        try {    
            System.out.println("");
            System.out.println("-----------------Andorra--------------------");
            System.out.println("");
            
            FileManager fileManager= new FileManager();
             designMap = fileManager.loadFile( "./work/Andorra.rvme");
            System.out.println("AnthemPath:       "+designMap.getAnthemPath());
            System.out.println("BorderThickness:      "+designMap.getBorderThickness());
            System.out.println("RegionName:     "+designMap.getRegionName());
            System.out.println("FlagImagePath:      "+designMap.getFlagImagePath());
            System.out.println("LeaderImagePath  :    "+designMap.getLeaderImagePath());
            System.out.println("getPolygonsPath  :    "+designMap.getPolygonsPath());
            
            System.out.println("Subregions_have_capitals   : "+designMap.isSubregions_have_capitals());
            System.out.println("Subregions_have_flags    :  "+designMap.isSubregions_have_flags());
            System.out.println("Subregions_have_leaders  :    "+designMap.isSubregions_have_leaders());
            System.out.println("BorderRed  :    "+designMap.getBorderRed());
            System.out.println("BorderGreen  :    "+designMap.getBorderGreen());
            System.out.println("BorderBlue  :    "+designMap.getBorderBlue());
            System.out.println("BackgroundRed  :    "+designMap.getBackgroundRed());
            System.out.println("BackgroundGreen  :    "+designMap.getBackgroundGreen());
            System.out.println("BackgroundBlue  :    "+designMap.getBackgroundBlue());
            System.out.println("Height  :    "+designMap.getHeight());
            System.out.println("Width  :    "+designMap.getWidth());
            System.out.println("ZoomLevel  :    "+designMap.getZoomLevel());
            System.out.println("ParentDirectory  :    "+designMap.getParentDirectory());
            System.out.println("xFlagImageLocation  :    "+designMap.getFlagImageLocation().get(0));
            System.out.println("yFlagImageLocation  :    "+designMap.getFlagImageLocation().get(1));
            
            System.out.println("xLeaderImageLocation  :    "+designMap.getLeaderImageLocation().get(0));
            System.out.println("yLeaderImageLocation  :    "+designMap.getLeaderImageLocation().get(1));
            
            System.out.println("xgetScrollMapLocation  :    "+designMap.getScrollMapLocation().get(0));
            System.out.println("ygetScrollMapLocation  :    "+designMap.getScrollMapLocation().get(1));
            
            
              ArrayList<SubRegion> list      = designMap.getTableList();
              for( SubRegion subRegion :   list){
                  System.out.println("{");
                System.out.println("Name  :    "+subRegion.getName()) ;
                System.out.println("Capital  :    "+subRegion.getCapital()) ;
                System.out.println("Leader  :    "+subRegion.getLeader()) ;
                System.out.println("Blue  :    "+subRegion.getBlue()) ;
                System.out.println("Green  :    "+subRegion.getGreen());
                System.out.println("Red  :    "+subRegion.getRed()) ;
                  System.out.println("}");
                  System.out.println("");
              
              }
            
            
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
		}
        return designMap;
    }
    public static DesignMap loadSanMarinoMap(){
        DataManager dataManager = new TestSave().dataManagerSan;
        DesignMap designMap = new DesignMap();
        try {
            System.out.println("");
            System.out.println("-----------------San Marino--------------------");
            System.out.println("");
            FileManager fileManager= new FileManager();
             designMap = fileManager.loadFile( "./work/San Marino.rvme");
            
            
            System.out.println("AnthemPath:       "+designMap.getAnthemPath());
            System.out.println("BorderThickness:      "+designMap.getBorderThickness());
            System.out.println("RegionName:     "+designMap.getRegionName());
            System.out.println("FlagImagePath:      "+designMap.getFlagImagePath());
            System.out.println("LeaderImagePath  :    "+designMap.getLeaderImagePath());
            System.out.println("getPolygonsPath  :    "+designMap.getPolygonsPath());
            
            System.out.println("Subregions_have_capitals   : "+designMap.isSubregions_have_capitals());
            System.out.println("Subregions_have_flags    :  "+designMap.isSubregions_have_flags());
            System.out.println("Subregions_have_leaders  :    "+designMap.isSubregions_have_leaders());
            System.out.println("BorderRed  :    "+designMap.getBorderRed());
            System.out.println("BorderGreen  :    "+designMap.getBorderGreen());
            System.out.println("BorderBlue  :    "+designMap.getBorderBlue());
            System.out.println("BackgroundRed  :    "+designMap.getBackgroundRed());
            System.out.println("BackgroundGreen  :    "+designMap.getBackgroundGreen());
            System.out.println("BackgroundBlue  :    "+designMap.getBackgroundBlue());
            System.out.println("Height  :    "+designMap.getHeight());
            System.out.println("Width  :    "+designMap.getWidth());
            System.out.println("ZoomLevel  :    "+designMap.getZoomLevel());
            System.out.println("ParentDirectory  :    "+designMap.getParentDirectory());
            System.out.println("xFlagImageLocation  :    "+designMap.getFlagImageLocation().get(0));
            System.out.println("yFlagImageLocation  :    "+designMap.getFlagImageLocation().get(1));
            
            System.out.println("xLeaderImageLocation  :    "+designMap.getLeaderImageLocation().get(0));
            System.out.println("yLeaderImageLocation  :    "+designMap.getLeaderImageLocation().get(1));
            
            System.out.println("xgetScrollMapLocation  :    "+designMap.getScrollMapLocation().get(0));
            System.out.println("ygetScrollMapLocation  :    "+designMap.getScrollMapLocation().get(1));
            
            ArrayList<SubRegion> list      = designMap.getTableList();
              for( SubRegion subRegion :   list){
                  System.out.println("{");
                System.out.println("Name  :    "+subRegion.getName()) ;
                System.out.println("Capital  :    "+subRegion.getCapital()) ;
                System.out.println("Leader  :    "+subRegion.getLeader()) ;
                System.out.println("Blue  :    "+subRegion.getBlue()) ;
                System.out.println("Green  :    "+subRegion.getGreen());
                System.out.println("Red  :    "+subRegion.getRed()) ;
                  System.out.println("}");
              
              }
            } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
		}
       return designMap;
    }
    public static DesignMap loadSlovakiaMap(){
        DataManager dataManager = new TestSave().dataManagerSlo;
        DesignMap designMap = new DesignMap();
        try {
            System.out.println("");
            System.out.println("-----------------Slovakia--------------------");
            System.out.println("");
            FileManager fileManager= new FileManager();
             designMap = fileManager.loadFile("./work/Slovakia.rvme");
            
            
            System.out.println("AnthemPath:       "+designMap.getAnthemPath());
            System.out.println("BorderThickness:      "+designMap.getBorderThickness());
            System.out.println("RegionName:     "+designMap.getRegionName());
            System.out.println("FlagImagePath:      "+designMap.getFlagImagePath());
            System.out.println("LeaderImagePath  :    "+designMap.getLeaderImagePath());
            System.out.println("getPolygonsPath  :    "+designMap.getPolygonsPath());
            
            System.out.println("Subregions_have_capitals   : "+designMap.isSubregions_have_capitals());
            System.out.println("Subregions_have_flags    :  "+designMap.isSubregions_have_flags());
            System.out.println("Subregions_have_leaders  :    "+designMap.isSubregions_have_leaders());
            System.out.println("BorderRed  :    "+designMap.getBorderRed());
            System.out.println("BorderGreen  :    "+designMap.getBorderGreen());
            System.out.println("BorderBlue  :    "+designMap.getBorderBlue());
            System.out.println("BackgroundRed  :    "+designMap.getBackgroundRed());
            System.out.println("BackgroundGreen  :    "+designMap.getBackgroundGreen());
            System.out.println("BackgroundBlue  :    "+designMap.getBackgroundBlue());
            System.out.println("Height  :    "+designMap.getHeight());
            System.out.println("Width  :    "+designMap.getWidth());
            System.out.println("ZoomLevel  :    "+designMap.getZoomLevel());
            System.out.println("ParentDirectory  :    "+designMap.getParentDirectory());
            System.out.println("xFlagImageLocation  :    "+designMap.getFlagImageLocation().get(0));
            System.out.println("yFlagImageLocation  :    "+designMap.getFlagImageLocation().get(1));
            
            System.out.println("xLeaderImageLocation  :    "+designMap.getLeaderImageLocation().get(0));
            System.out.println("yLeaderImageLocation  :    "+designMap.getLeaderImageLocation().get(1));
            
            System.out.println("xgetScrollMapLocation  :    "+designMap.getScrollMapLocation().get(0));
            System.out.println("ygetScrollMapLocation  :    "+designMap.getScrollMapLocation().get(1));
            
            ArrayList<SubRegion> list      = designMap.getTableList();
              for( SubRegion subRegion :   list){
                  System.out.println("{");
                System.out.println("Name  :    "+subRegion.getName()) ;
                System.out.println("Capital  :    "+subRegion.getCapital()) ;
                System.out.println("Leader  :    "+subRegion.getLeader()) ;
                System.out.println("Blue  :    "+subRegion.getBlue()) ;
                System.out.println("Green  :    "+subRegion.getGreen());
                System.out.println("Red  :    "+subRegion.getRed()) ;
                  System.out.println("}");
              
              }
            } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
		}
        return designMap;
    }
        
 }
    
    
    
    
    
    
    
    
    
    
    
    
    
    

