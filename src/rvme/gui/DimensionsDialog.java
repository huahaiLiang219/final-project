/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvme.gui;

import java.io.File;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.management.RuntimeErrorException;
import properties_manager.PropertiesManager;
import rvme.PropertyType;
import static saf.settings.AppStartupConstants.PATH_WORK;

/**
 *
 * @author admin
 */
public class DimensionsDialog {
    public PropertiesManager props;
    
    Button yesButton ;
    Button cancelButton ;
    
     Label LHeight;
    Label LWidth;
    boolean wflag=false;
    boolean hflag=false;
    TextField height=new TextField() ;
    TextField width=new TextField() ;
     public  String selection;
     
     public void showDialog(){
               props = PropertiesManager.getPropertiesManager();
                final Stage dialog = new Stage();
                dialog.initModality(Modality.APPLICATION_MODAL);
                GridPane gridPane = new GridPane();
               
               //dialogScene.getStylesheets().add("tdlm/css/tdlm_style.css");
               gridPane.getStylesheets().add(props.getProperty(PropertyType.CSS_PATH));
               gridPane.getStyleClass().add(props.getProperty(PropertyType.GRID_PANE));
                
                

                //make the add items table
                 yesButton =new Button(props.getProperty(PropertyType.YES));
                 cancelButton =new Button(props.getProperty(PropertyType.CANCEL));
                
                 
                 LHeight=  new Label(props.getProperty(PropertyType.HEIGHT));
                 LWidth=  new Label(props.getProperty(PropertyType.WIDTH));
                //set Label style from css file
                setLabelStyle(LHeight);
                setLabelStyle(LWidth);
                 
                 yesButton =new Button(props.getProperty(PropertyType.YES));
                 cancelButton =new Button(props.getProperty(PropertyType.CANCEL));
                yesButton.setDisable(true);
                
                gridPane.add(LHeight, 0, 0);
                gridPane.add(height, 1, 0);
                
                gridPane.add(LWidth, 0, 1);
                gridPane.add(width, 1, 1);
                
                gridPane.add(yesButton, 0, 2);
                gridPane.add(cancelButton, 1, 2);
                 
                 GridPane.setRowSpan(yesButton, 3);
                GridPane.setRowSpan(cancelButton, 3);
                 
                yesButton.setOnAction(e->{
                     selection=props.getProperty(PropertyType.YES);
                     dialog.close();
                         });
                 cancelButton.setOnAction(e->{
                     selection=props.getProperty(PropertyType.CANCEL);
                     dialog.close();});
                 
                 
                 height.textProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                try {
                    if(!height.getText().equals("")){
                   double heightH = Double.parseDouble(height.getText());
                    }
                    hflag=false;
                updateYesButton();
                } catch (java.lang.NumberFormatException e) {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Error Dialog");
                alert.setHeaderText("Attention,There was an error");
                alert.setContentText("Please enter the numbers!");
                alert.showAndWait();
                yesButton.setDisable(true);
                     hflag=true;
                }
            }
        });
                 
                 width.textProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                try {
                    if(!width.getText().equals("")){
                   double weightW = Double.parseDouble(width.getText());
                    }
                    wflag=false;
                updateYesButton();
                } catch (java.lang.NumberFormatException e) {
                 Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Error Dialog");
                alert.setHeaderText("Attention,There was an error");
                alert.setContentText("Please enter the numbers!");
                wflag=true;
                alert.showAndWait();
                yesButton.setDisable(true);
                }
            }
        });
                 dialog.setTitle(props.getProperty(PropertyType.DimisionsDialog_TITLE));
                 Scene dialogScene = new Scene(gridPane, 350, 180);
                 dialog.setScene(dialogScene);
                dialog.showAndWait();
      
 }
     
     private void updateYesButton() {
         boolean flag = false;
          String textHeight = height.getText();
          String textWidth = width.getText();
          if(textHeight.isEmpty()||textWidth.isEmpty()){
              flag=true;
          }
          if(wflag||hflag){
           flag=true;
          }
          yesButton.setDisable(flag);
     }
     public void setSelection(String sel) {
        this.selection=sel;
    }
    public String getSelection() {
        return selection;
    }
    public String getHeight(){
        return height.getText();
    }
    
    public String getWidth(){
        return width.getText();
    }
    
    public void setLabelStyle(Label label){
        label.getStylesheets().add(props.getProperty(PropertyType.CSS_PATH));
        label.getStyleClass().add(props.getProperty(PropertyType.FONT));
    }
}
