package rvme.gui;


import java.io.File;
import java.io.IOException;
import java.util.Optional;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import rvme.controller.MapController;
import rvme.data.DataManager;
import saf.ui.AppYesNoCancelDialogSingleton;
import saf.ui.AppMessageDialogSingleton;
import properties_manager.PropertiesManager;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import rvme.PropertyType;
import static rvme.PropertyType.NEXT_REGION_ICON;
import static rvme.PropertyType.PREVIOUS_REGION_ICON;
import rvme.data.DesignMap;
import rvme.data.SubRegion;
import rvme.data.SubRegionForTable;
import static rvme.gui.SubRegionDialog.FILE_PROTOCOL;
import static rvme.gui.SubRegionDialog.PATH_IMAGES;
import static saf.settings.AppPropertyType.LOAD_ICON;
import static saf.settings.AppPropertyType.LOAD_WORK_TITLE;

/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna  && Huahai Liang
 * @version 1.0
 */
public class Workspace extends AppWorkspaceComponent {

    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS Workspace'S COMPONENTS TO A STYLE SHEET THAT IT USES
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    static final String CLASS_SLIDER = "slider";
    static final String EMPTY_TEXT = "";
    static final int LARGE_TEXT_FIELD_LENGTH = 20;
    static final int SMALL_TEXT_FIELD_LENGTH = 5;

    SubRegionDialog sbd;
    Button next;
    Button previous;
    
    double orgSceneX, orgSceneY;
    double orgTranslateX, orgTranslateY;
    
    Pane groupPane;
    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;
    StringBuffer sb = new StringBuffer();
    // THIS CONTROLLER PROVIDES THE RESPONSES TO INTERACTIONS
    MapController toDoListController;
    
     StackPane leftp;
    // THIS IS OUR WORKSPACE HEADING
    Pane rightp;
    Label headingLabel;
    Group group;
     // THIS REGION IS FOR MANAGING TODO ITEMS
    VBox itemsBox;
    Label itemsLabel;
    HBox itemsToolbar;
    VBox hbox;
    
    VBox zoombox;
    VBox thicknessbox;
    
    
    VBox bgcbox= new VBox();;
    VBox bdcbox= new VBox();;
    
    public ColorPicker backgoundColorPicker = new ColorPicker();
    public  ColorPicker borderColorPicker = new ColorPicker();
    
    //Edit ToolBar's  Buttons
    Button addImageButton;
    Button removeImageButton;
    Button changeMapNameButton;
    Button reassighMapColorsButton;
    Button playAnthemButton;
    Button changeMapDimensionsButton;
    
    Label zoomSliderLabel;
    Label changeBorderThicknessLabel;
    
    Label bgcLabel= new Label();;
    Label bdcLabel= new Label();;
    
    public Slider zoomSlider;
    public Slider changeBorderThickness;
    
    
    TableView<SubRegionForTable> itemsTable;
    TableColumn itemRegionNameColumn;
    TableColumn itemCapitalColumn;
    TableColumn itemLeaderNameColumn;

    // HERE ARE OUR DIALOGS
    AppMessageDialogSingleton messageDialog;
    AppYesNoCancelDialogSingleton yesNoCancelDialog;
    
    // FOR DISPLAYING DEBUG STUFF
    Text debugText;
    
    DataManager dataManager;

    
    private double sceneX, sceneY, translateX, translateY;
    ImageView flagIV;
    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
	// KEEP THIS FOR LATER
	app = initApp;
            dataManager=(DataManager)app.getDataComponent();
            changeBorderThickness= new Slider();
            zoomSlider= new Slider();
	// KEEP THE GUI FOR LATER
	gui = app.getGUI();

        // INIT ALL WORKSPACE COMPONENTS
	layoutGUI();
        
        // AND SETUP EVENT HANDLING
	setupHandlers();
    }
    public Pane getMapPane(){return  rightp;}
    private void layoutGUI() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
	// FIRST THE LABEL AT THE TOP
        headingLabel = new Label();
        headingLabel.setText(props.getProperty(PropertyType.WORKSPACE_HEADING_LABEL)); 
        
        zoomSliderLabel = new Label();
        changeBorderThicknessLabel = new Label();
        zoomSliderLabel.setText(props.getProperty(PropertyType.ZOOM_SLIDER_LABEL)); 
        changeBorderThicknessLabel.setText(props.getProperty(PropertyType.CHANGE_BORDER_THICKNESS_LABEL)); 
       
        bgcLabel.setText(props.getProperty(PropertyType.BGC_LABEL)); 
        bdcLabel.setText(props.getProperty(PropertyType.BDC_LABEL)); 
        
        
        
        // NOW THE CONTROLS FOR ADDING LECTURES
        itemsBox = new VBox();
        itemsLabel = new Label(props.getProperty(PropertyType.ITEMS_HEADING_LABEL));
        itemsToolbar = new HBox();

        //Map and Table 
        addImageButton = initChildButton(itemsToolbar, PropertyType.ADD_IMAGE_ICON.toString(), PropertyType.ADD_IMAGE_BUTTON_TOOLTIP.toString(), false);
        removeImageButton = initChildButton(itemsToolbar, PropertyType.REMOVE_IMAGE_ICON.toString(), PropertyType.REMOVE_IMAGE_BUTTON_TOOLTIP.toString(), false);
        
        changeMapNameButton = initChildButton(itemsToolbar, PropertyType.CHANGE_MAP_NAME_ICON.toString(), PropertyType.CHANGE_MAP_NAME_BUTTON_TOOLTIP.toString(), false);
        reassighMapColorsButton = initChildButton(itemsToolbar, PropertyType.REASSIGH_MAP_COLORS_ICON.toString(), PropertyType.REASSIGH_MAP_COLORS_BUTTON_TOOLTIP.toString(), false);
        playAnthemButton = initChildButton(itemsToolbar, PropertyType.PLAY_ANTHEM_BUTTON_ICON.toString(), PropertyType.PLAY_ANTHEM_BUTTON_TOOLTIP.toString(), false);
        changeMapDimensionsButton = initChildButton(itemsToolbar, PropertyType.CHANGE_MAP_DIMENSIONS_ICON.toString(), PropertyType.CHANGE_MAP_DIMENSIONS_BUTTON_TOOLTIP.toString(), false);
        
        //Slider  and  VBOX
         zoombox= new VBox();
        thicknessbox= new VBox();
        
        
        
        //set slider style
        zoomSlider.setMin(0);
        zoomSlider.setMax(5);
        zoomSlider.setValue(1);
        zoomSlider.setShowTickLabels(true);
        zoomSlider.setShowTickMarks(true);
        zoomSlider.setMajorTickUnit(50);
        zoomSlider.setMinorTickCount(5);
        zoomSlider.setBlockIncrement(10);
        
        changeBorderThickness.setMin(0.5);
        changeBorderThickness.setMax(6);
        changeBorderThickness.setValue(0.5);
        changeBorderThickness.setShowTickLabels(true);
        changeBorderThickness.setShowTickMarks(true);
        changeBorderThickness.setMajorTickUnit(50);
        changeBorderThickness.setMinorTickCount(5);
        changeBorderThickness.setBlockIncrement(10);
        
        
        zoombox.getChildren().add(zoomSliderLabel);
        zoombox.getChildren().add(zoomSlider);
        
        thicknessbox.getChildren().add(changeBorderThicknessLabel);
        thicknessbox.getChildren().add(changeBorderThickness);
        
        //add to Toolbar
        backgoundColorPicker.setValue(Color.LIGHTBLUE);
        borderColorPicker.setValue(Color.LIGHTBLUE);
        
         bgcbox.getChildren().addAll(bgcLabel,backgoundColorPicker);
         bdcbox.getChildren().addAll(bdcLabel,borderColorPicker);
         
        itemsToolbar.getChildren().add(bgcbox);
        itemsToolbar.getChildren().add(bdcbox);
        
        itemsToolbar.getChildren().add(zoombox);
        itemsToolbar.getChildren().add(thicknessbox);
        
        
        //table
        itemsTable = new TableView();
        itemsBox.getChildren().add(itemsLabel);
        itemsBox.getChildren().add(itemsToolbar);
//      itemsBox.getChildren().add(itemsTable);

        // NOW SETUP THE TABLE COLUMNS
        itemRegionNameColumn = new TableColumn(props.getProperty(PropertyType.REGIONNAME_COLUMN_HEADING));
        itemCapitalColumn = new TableColumn(props.getProperty(PropertyType.CAPITAL_COLUMN_HEADING));
        itemLeaderNameColumn = new TableColumn(props.getProperty(PropertyType.LEADERNAME_COLUMN_HEADING));
        
        // AND LINK THE COLUMNS TO THE DATA
//        StringProperty nameForTable;
//     StringProperty capitalForTable;
//     StringProperty leaderForTable;
        itemRegionNameColumn.setCellValueFactory(new PropertyValueFactory<SubRegionForTable, String>("nameForTable"));
        itemCapitalColumn.setCellValueFactory(new PropertyValueFactory<SubRegionForTable, String>("capitalForTable"));
        itemLeaderNameColumn.setCellValueFactory(new PropertyValueFactory<SubRegionForTable, String>("leaderForTable"));
        
        itemRegionNameColumn.prefWidthProperty().bind(itemsTable.widthProperty().multiply(0.1));
        itemCapitalColumn.prefWidthProperty().bind(itemsTable.widthProperty().multiply(0.1));
        itemLeaderNameColumn.prefWidthProperty().bind(itemsTable.widthProperty().multiply(0.1));
        
        itemsTable.getColumns().add(itemRegionNameColumn);
        itemsTable.getColumns().add(itemCapitalColumn);
        itemsTable.getColumns().add(itemLeaderNameColumn);
        ObservableList<SubRegionForTable> data = FXCollections.observableArrayList(
//                new SubRegion("Jacob", "capital1", "leader1","","",1,1,1),
//                new SubRegion("Isabella", "capital2", "leader2","","",1,1,1),
//                new SubRegion("Ethan", "capital3", "leader3","","",1,1,1),
//                new SubRegion("Emma", "capital4", "leader4","","",1,1,1),
//                new SubRegion("Michael", "capital5", "leader5","","",1,1,1),
//                new SubRegion("sample", "sample", "sample","","",1,1,1)
            );
         itemsTable.setItems(data);
        //Set DataManager
        
      DataManager dataManager = (DataManager)app.getDataComponent();
//      dataManager.setItem(itemsTable.getItems());
      dataManager.setSubRegionForTable(itemsTable.getItems());
        //itemsTable.setItems(dataManager.getItems());  
        
        //Set up Split Pane
        itemsTable.setPrefSize(1800,1200);
         leftp = new StackPane();
         
          groupPane= new Pane();
         
          group = new Group();
         rightp = new Pane();
        hbox = new VBox(20);
        SplitPane splitPane = new SplitPane();
//        splitPane.setPrefSize(1000, 600);
//        System.out.println(leftp.getWidth());
//        System.out.println(leftp.getHeight());
        
//        groupPane.getChildren().add(group);
//        leftp.getChildren().add(groupPane);
        
//         groupPane.centerProperty().set(group);
            leftp.getChildren().add(group);
//         leftp.centerProperty().set(group);


        rightp.getChildren().addAll(itemsTable);
        leftp.setStyle("-fx-background-color:white; -fx-opacity:1;");
//        rightp.setStyle("-fx-background-color:white; -fx-opacity:1;");
        
//         Rectangle clipRect = new Rectangle(leftp.getWidth(), leftp.getHeight());
//        clipRect.heightProperty().bind(leftp.heightProperty());
//        clipRect.widthProperty().bind(leftp.widthProperty());
//        leftp.setClip(clipRect);

        splitPane.getItems().addAll(leftp, rightp);
//        splitPane.getItems().get(0).minWidth(1.0);
        ObservableList<SplitPane.Divider> dividers = splitPane.getDividers();
//        leftp.setMaxSize(700, 700);
//        leftp.maxWidthProperty().bind(splitPane.widthProperty().multiply(0.64));
        dividers.get(0).setPosition(0.64);

        hbox.getChildren().add(splitPane);
        
	// AND NOW SETUP THE WORKSPACE
	workspace = new VBox();
//        workspace.getChildren().add(headingLabel);
        workspace.getChildren().add(itemsBox);
        workspace.getChildren().add(hbox);
        
        
        
        next = new Button(props.getProperty(PropertyType.Next));
        previous = new Button(props.getProperty(PropertyType.Previous));
        
         String neximagePath = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(NEXT_REGION_ICON.toString());
         String preimagePath = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(PREVIOUS_REGION_ICON.toString());
        Image nextbuttonImage = new Image(neximagePath);
        Image prebuttonImage = new Image(preimagePath);
	
	// NOW MAKE THE BUTTON
        next.setGraphic(new ImageView(nextbuttonImage));
        previous.setGraphic(new ImageView(prebuttonImage));
        next.setOnAction(e->{
            itemsTable.getSelectionModel().selectNext();
            SubRegionForTable subRegionForTable=  itemsTable.getSelectionModel().getSelectedItem();
                sbd.setRegionName(subRegionForTable.getNameForTable());
                sbd.setCapital(subRegionForTable.getCapitalForTable());
                sbd.setLeaderName(subRegionForTable.getLeaderForTable());
            });
        previous.setOnAction(e->{
            itemsTable.getSelectionModel().selectPrevious();
            SubRegionForTable subRegionForTable=  itemsTable.getSelectionModel().getSelectedItem();
                sbd.setRegionName(subRegionForTable.getNameForTable());
                sbd.setCapital(subRegionForTable.getCapitalForTable());
                sbd.setLeaderName(subRegionForTable.getLeaderForTable());

            });
    }
    public Pane getGroupPane(){return groupPane;}
    public StackPane getLeftP(){return leftp;}
    public void setDebugText(String text) {
	debugText.setText(text);
    }
    
    
    
    
     EventHandler<MouseEvent> clickedEvent = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent e) {
            if (e.getButton().equals(MouseButton.PRIMARY)){
                group.setScaleX(((DataManager)app.getDataComponent()).getScaleValue()*2);
            }else if (e.getButton().equals(MouseButton.SECONDARY)){
                group.setScaleX(((DataManager)app.getDataComponent()).getScaleValue()/2);
            } 
        }
    };
    
     
     
      EventHandler<MouseEvent> circleOnMousePressedEventHandler = 
        new EventHandler<MouseEvent>() {
 
        @Override
        public void handle(MouseEvent t) {
            orgSceneX = t.getSceneX();
            orgSceneY = t.getSceneY();
            orgTranslateX = ((Group)(t.getSource())).getTranslateX();
            orgTranslateY = ((Group)(t.getSource())).getTranslateY();
            
             app.getGUI().updateToolbarControls(false);
                app.getGUI().updateExportButton(false);
        }
    };
     
    EventHandler<MouseEvent> circleOnMouseDraggedEventHandler = 
        new EventHandler<MouseEvent>() {
 
        @Override
        public void handle(MouseEvent t) {
            double offsetX = t.getSceneX() - orgSceneX;
            double offsetY = t.getSceneY() - orgSceneY;
            double newTranslateX = orgTranslateX + offsetX;
            double newTranslateY = orgTranslateY + offsetY;
             
            ((Group)(t.getSource())).setTranslateX(newTranslateX);
            ((Group)(t.getSource())).setTranslateY(newTranslateY);
            
             app.getGUI().updateToolbarControls(false);
                app.getGUI().updateExportButton(false);
        }
    };
     
     
    private void setupHandlers() {
    
	toDoListController = new MapController(app);
       
        group.setOnMouseClicked(clickedEvent);
         
         
        app.getGUI().getAppPane().setOnKeyPressed(new EventHandler<KeyEvent>() {
        @Override
        public void handle(KeyEvent key) {
            
            if (key.getCode()==KeyCode.UP){
//        itemsTable.getSelectionModel().clearSelection();
                leftp.requestFocus();
                double translateY =((DataManager)app.getDataComponent()).translateY;
                translateY= translateY+20;
                System.out.println("1 UP");
                group.setTranslateY(translateY);
            }
            else if (key.getCode()==KeyCode.DOWN){
                leftp.requestFocus();
                System.out.println("1 DOWN");
                group.setTranslateY(20);
            }
            else if (key.getCode()==KeyCode.LEFT){
                leftp.requestFocus();
                System.out.println("1 LEFT");
                group.setTranslateX(20);
            }
            else if (key.getCode()==KeyCode.RIGHT)
                leftp.requestFocus();
                group.setTranslateX(group.getTranslateX()-20);
        }
    });
	// MAKE THE CONTROLLER
	
	// NOW CONNECT THE BUTTONS TO THEIR HANDLERS
       addImageButton.setOnAction(e->{
             PropertiesManager props = PropertiesManager.getPropertiesManager();
              FileChooser fc = new FileChooser();
                   fc.setInitialDirectory(new File(PATH_IMAGES));
                   fc.setTitle(props.getProperty(LOAD_WORK_TITLE));
                    File dataFile = fc.showOpenDialog(app.getGUI().getWindow());
                System.out.println(dataFile);
            
                
                String flaP=FILE_PROTOCOL + dataFile.getAbsolutePath();
                   System.out.println(flaP);
                   Image flagI = new Image(flaP);
                   
                    flagIV= new ImageView();
//                    flagIV.setFitHeight(200);
//                    flagIV.setFitWidth(80);
                    flagIV.setImage(flagI);
                    flagIV.setOnMousePressed(press);
                    flagIV.setOnMouseDragged(drag);
                leftp.getChildren().add(flagIV);
                app.getGUI().updateToolbarControls(false);
                app.getGUI().updateExportButton(false);
//            toDoListController.processAddImage();

        });
        removeImageButton.setOnAction(e->{
                    Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText("Attention! a Confirmation Dialog");
        alert.setContentText("Are you delete image?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            
            leftp.getChildren().remove(flagIV);
        } else {
            
        }
            
            
            
                app.getGUI().updateToolbarControls(false);
                app.getGUI().updateExportButton(false);
//            toDoListController.processRemoveImage();
        });
        changeMapNameButton.setOnAction(e->{
            toDoListController.processChangeMapName();
        });
        
        playAnthemButton.setOnAction(e->{
//            toDoListController.processPlayAnthemButton();
        DesignMap designMap = dataManager.getDesignMap();
        
        
//         FileChooser fc = new FileChooser();
//         fc.setInitialDirectory(new File(PATH_IMAGES));
//         File dataFile = fc.showOpenDialog(app.getGUI().getWindow());
//        
         
//        String path=dataFile.getAbsolutePath();
//        designMap.setAnthemPath(path);
            Media media= new Media("file:///"+"C:/Users/admin/Desktop/Stony Brook/CSE 219/Summer HomeWork/RegioVincoMapEditor/Andorra National Anthem.mp3");
            MediaPlayer player= new MediaPlayer(media);
            
            player.play();
            MediaView mediaView = new MediaView(player);
        app.getGUI().getAppPane().getChildren().add(mediaView);
            
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("I have a great message for you!");

            alert.showAndWait();
            
            player.pause();
            
        });
        
        changeMapDimensionsButton.setOnAction(e->{
            toDoListController.processChangeMapDimensionsButton();
        });
        reassighMapColorsButton.setOnAction(e->{
//            toDoListController.processReassighMapColorsButton();

         DesignMap designMap = dataManager.getDesignMap();
            //store into dataManager for saving file later
            
                int color =(int)(Math.random()*160);
            
           Group group= (Group) getLeftP().getChildren().get(0);
            
            //set up backgroundColor
             ObservableList<Node> polygons=group.getChildren();
                for(Node node: polygons){
                Polygon plg = (Polygon)node;
             plg.setFill(Color.rgb(color,color,color));
                color=color+5;
            }
                app.getGUI().updateToolbarControls(false);
                app.getGUI().updateExportButton(false);
        });
        
        backgoundColorPicker.setOnAction(e->{
            Color bgc= backgoundColorPicker.getValue();
            
            DesignMap designMap = dataManager.getDesignMap();
            //store into dataManager for saving file later
            designMap.setBackgroundRed((int)bgc.getRed()*255);
            designMap.setBackgroundGreen((int)bgc.getGreen()*255);
            designMap.setBackgroundBlue((int)bgc.getBlue()*255);
            //set up backgroundColor
//            String colorString= bgc.toString().substring(2,8);
//            leftp.setStyle("-fx-background-color: #" +colorString);
            leftp.setBackground(new Background(new BackgroundFill(bgc,null,null)));
//            System.out.println(colorString);
            app.getGUI().updateToolbarControls(false);
                app.getGUI().updateExportButton(false);
//            toDoListController.processChangeBackgroundColors();
        });
        
        borderColorPicker.setOnAction(e->{
         Color bgc= borderColorPicker.getValue();
         
         DesignMap designMap = dataManager.getDesignMap();
            //store into dataManager for saving file later
            designMap.setBorderRed((int)bgc.getRed()*255);
            designMap.setBorderGreen((int)bgc.getGreen()*255);
            designMap.setBorderBlue((int)bgc.getBlue()*255);
            
//            Group group =(Group) ((BorderPane)leftp.getChildren().get(0)).getCenter();
//            Group group =(Group)leftp.getCenter();
//           Group group= (Group) getGroupPane().getChildren().get(0);
           Group group= (Group) getLeftP().getChildren().get(0);
            
            //set up backgroundColor
             ObservableList<Node> polygons=group.getChildren();
                for(Node node: polygons){
                Polygon plg = (Polygon)node;
             plg.setStroke(bgc);
            }
                app.getGUI().updateToolbarControls(false);
                app.getGUI().updateExportButton(false);
        //            toDoListController.processChangeBorderColors();
        });
        
        changeBorderThickness.valueProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                //set up changeBorderThickness
                double Thickness = changeBorderThickness.getValue();
                DesignMap designMap = dataManager.getDesignMap();
                designMap.setBorderThickness(Thickness);
                 
//              Group group =(Group) leftp.getChildren().get(0);
//              Group group =(Group)leftp.getCenter();
//            Group group= (Group) getGroupPane().getChildren().get(0);
Group group= (Group) getLeftP().getChildren().get(0);
             ObservableList<Node> polygons=group.getChildren();
                for(Node node: polygons){
                Polygon plg = (Polygon)node;
                plg.setStrokeWidth(Thickness);
              }
                app.getGUI().updateToolbarControls(false);
                app.getGUI().updateExportButton(false);
            }
        });
        
        
        zoomSlider.valueProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                DesignMap designMap = dataManager.getDesignMap();
                double zoonL=zoomSlider.getValue();
                
                designMap.setZoomLevel(zoonL);
                
//                 Group group =(Group) ((BorderPane)leftp.getChildren().get(0)).getCenter();
//                 Group group =(Group)leftp.getCenter();
//                Group group= (Group) getGroupPane().getChildren().get(0);
Group group= (Group) getLeftP().getChildren().get(0);
                    
                 group.setScaleX(zoonL);
                 group.setScaleY(zoonL);
                 app.getGUI().updateToolbarControls(false);
                app.getGUI().updateExportButton(false);
            }
            
        });
        
        itemsTable.setRowFactory(r -> {
            TableRow row = new TableRow<>();
            //foolproof design
            row.setOnMouseClicked(clicked->{
            //toDoListController.processDisableButton();
            
            if (clicked.getClickCount() ==2&& !row.isEmpty()) {  
                sbd=  new SubRegionDialog(app);
                toDoListController.processEditItem(sbd);
            }
            });
            return row;
        });
        
        
        leftp.setOnMouseClicked(new EventHandler<MouseEvent>() {
    @Override
    public void handle(MouseEvent mouseEvent) {
        if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
            if(mouseEvent.getClickCount() == 1){
//                Polygon plg =(Polygon) mouseEvent.getTarget();
//                plg.setStrokeWidth(7);
//                plg.setFill(Color.GREEN);
//                plg.setStroke(Color.LIGHTBLUE);
                System.out.println("single clicked");
            }
            if(mouseEvent.getClickCount() == 2){
                System.out.println("Double clicked");
            }
        }
    }
});
          
    }
    
    public void setImage(ButtonBase button, String fileName) {
	// LOAD THE ICON FROM THE PROVIDED FILE
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + fileName;
        Image buttonImage = new Image(imagePath);
	
	// SET THE IMAGE IN THE BUTTON
        button.setGraphic(new ImageView(buttonImage));	
    }

    public Button getNextBt(){
        return next;
    }
    
    public Button getPreBt(){
        return previous;
    }
    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    @Override
    public void initStyle() {
	// NOTE THAT EACH CLASS SHOULD CORRESPOND TO
	// A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
	// CSS FILE
        
        // FIRST THE WORKSPACE PANE
        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        // THEN THE HEADING
	headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
	zoomSliderLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
	changeBorderThicknessLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
	hbox.getStyleClass().add(CLASS_BORDERED_PANE);
        
        // THEN THE DETAILS PANE AND ITS COMPONENTS
        itemsBox.getStyleClass().add(CLASS_BORDERED_PANE);
        
        zoombox.getStyleClass().add(CLASS_SLIDER);
        thicknessbox.getStyleClass().add(CLASS_SLIDER);
        itemsLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
        bgcLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
        bdcLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
    }
    public Button initChildButton(Pane toolbar, String icon, String tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	
	// LOAD THE ICON FROM THE PROVIDED FILE
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(icon);
        Image buttonImage = new Image(imagePath);
	
	// NOW MAKE THE BUTTON
        Button button = new Button();
        button.setDisable(disabled);
        ImageView im=new ImageView();
        im.setFitWidth(32);
        im.setFitHeight(34);
        
        im.setImage(buttonImage);
        button.setGraphic(im);
        
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip));
        button.setTooltip(buttonTooltip);
        
	// PUT THE BUTTON IN THE TOOLBAR
        toolbar.getChildren().add(button);
	
	// AND RETURN THE COMPLETED BUTTON
        return button;
    }
    
    
    public void setupWorkSpace(Color bgc,Color bdc,double zoom,double thickness){
    backgoundColorPicker.setValue(bgc);
    borderColorPicker.setValue(bdc);
    
    zoomSlider.setValue(zoom);
    changeBorderThickness.setValue(thickness);
    }
    /**
     * This function reloads all the controls for editing tag attributes into
     * the workspace.
     */
    @Override
    public void reloadWorkspace() {
	DataManager dataManager = (DataManager)app.getDataComponent();
        //dataManager.getSubRegion().clear();
//        leftp.getChildren().clear();
        leftp.getStyleClass().clear();
        
    }
    
    private EventHandler<MouseEvent> press = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent press) {
            flagIV = (ImageView)press.getSource();
            sceneX = press.getSceneX();
            sceneY = press.getSceneY();
            translateX = flagIV.getTranslateX();
            translateY = flagIV.getTranslateY();
             app.getGUI().updateToolbarControls(false);
                app.getGUI().updateExportButton(false);
            
        }
    };
    
    private EventHandler<MouseEvent> drag = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent drag) {
            double offsetX = drag.getSceneX() - sceneX;
            double offsetY = drag.getSceneY() - sceneY;
            flagIV.setTranslateX(translateX + offsetX);
            flagIV.setTranslateY(translateY + offsetY);  
             app.getGUI().updateToolbarControls(false);
                app.getGUI().updateExportButton(false);
        }
    };
    
    
    
    public TableView<SubRegionForTable> getTable(){
        return itemsTable;
    }
    
    public void setSBD(SubRegionDialog sbd){
        this.sbd = sbd;
    }
    
    public MapController getController(){
        return toDoListController;
    }
    
    public SubRegionDialog getSBD(){
        return sbd;
    }
    
}