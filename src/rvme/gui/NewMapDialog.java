/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvme.gui;

import java.io.File;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import saf.AppTemplate;
import saf.PropertyType;
import static saf.settings.AppPropertyType.DataFile_TOOLTIP;
import static saf.settings.AppPropertyType.Dir_TOOLTIP;
import static saf.settings.AppPropertyType.LOAD_ICON;
import static saf.settings.AppPropertyType.LOAD_WORK_TITLE;
import static saf.settings.AppStartupConstants.PATH_EXPORT;
import static saf.settings.AppStartupConstants.PATH_WORK;
import static saf.settings.AppStartupConstants.PATH_raw_map_data;

/**
 *
 * @author admin
 */
public class NewMapDialog {
    public static final String FILE_PROTOCOL = "file:";
    public static final String PATH_IMAGES = "./images/";
    AppTemplate app ;
    public PropertiesManager props;
    
    Button yesButton ;
    Button cancelButton ;
    
    
    Button directoryChooser ;
    Button dataFileChooser ;
    Stage stage;
    
     Label LregionName;
    Label Ldirectory;
    Label LdataPath;
    
    TextField regionName ;
    TextField directory ;
    TextField dataPath ;
    String dir;
    String filePath;
     public  String selection;
     String oldRegionName;
     public NewMapDialog(AppTemplate ap){
         stage=ap.getGUI().getWindow();
         app=ap;
     regionName=new TextField() ;
     directory=new TextField() ;
     dataPath=new TextField() ;
     directoryChooser=new Button(); 
     dataFileChooser=new Button() ;
//         showDialog();
     }
     
     public NewMapDialog(Stage sg){
         stage = sg;
//         showDialog();
     }
     public void setDialogValues(String rg,String dir,String dataFile){
         this.oldRegionName=rg;
      this.getRegionText().setText(rg);
      this.getDirectory().setText(dir);
      this.getDataPath().setText(dataFile);
     }
     public NewMapDialog(){}
     
     public TextField getRegionText(){return regionName;}
     public TextField getDirectory(){return directory;}
     public TextField getDataPath(){return dataPath;}
     
     public void showDialog(){
               props = PropertiesManager.getPropertiesManager();
                 Stage dialog = new Stage();
                dialog.initModality(Modality.APPLICATION_MODAL);
                GridPane gridPane = new GridPane();
               
               //dialogScene.getStylesheets().add("tdlm/css/tdlm_style.css");
               gridPane.getStylesheets().add(props.getProperty(PropertyType.CSS_PATH));
               gridPane.getStyleClass().add(props.getProperty(PropertyType.GRID_PANE));

                //make the add items table
//                yesButton =new Button(props.getProperty(PropertyType.YES));
//                
//               
//                
//                cancelButton =new Button(props.getProperty(PropertyType.CANCEL));
                 
	// LOAD THE ICON FROM THE PROVIDED FILE
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(LOAD_ICON.toString());
        Image buttonImage = new Image(imagePath);
	
	// NOW MAKE THE BUTTON
        directoryChooser.setGraphic(new ImageView(buttonImage));
        dataFileChooser.setGraphic(new ImageView(buttonImage));
        
        Tooltip dirButtonTooltip = new Tooltip(props.getProperty(Dir_TOOLTIP.toString()));
        Tooltip FileButtonTooltip = new Tooltip(props.getProperty(DataFile_TOOLTIP.toString()));
        directoryChooser.setTooltip(dirButtonTooltip);
        dataFileChooser.setTooltip(FileButtonTooltip);
	
                
                 //initial Label
                 LregionName=  new Label(props.getProperty(PropertyType.LRegionName));
                 Ldirectory=  new Label(props.getProperty(PropertyType.Ldirectory));
                 LdataPath=  new Label(props.getProperty(PropertyType.LdataPath));
                 
                //set Label style from css file
                setLabelStyle(LregionName);
                setLabelStyle(Ldirectory);
                setLabelStyle(LdataPath);
                 
                 yesButton =new Button(props.getProperty(PropertyType.YES));
                 cancelButton =new Button(props.getProperty(PropertyType.CANCEL));
                
                 yesButton.setDisable(true);
                gridPane.add(LregionName, 0, 0);
                gridPane.add(regionName, 1, 0);
                directory.setEditable(false);
                dataPath.setEditable(false);
                
                gridPane.add(Ldirectory, 0, 1);
                gridPane.add(directory, 1, 1);
                gridPane.add(directoryChooser, 2, 1);
                
                gridPane.add(LdataPath, 0, 2);
                gridPane.add(dataPath, 1, 2);
                gridPane.add(dataFileChooser, 2, 2);
                 
                gridPane.add(yesButton, 0, 3);
                gridPane.add(cancelButton, 1, 3);
                
                 GridPane.setRowSpan(yesButton, 3);
                GridPane.setRowSpan(cancelButton, 3);
                 
                regionName.textProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                File file =new File(PATH_WORK+newValue+".rvme");
                if(file.exists()){
                 Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText("The Region Name is already exist! Please Rename it!");
                alert.showAndWait();
                yesButton.setDisable(true);
                }    
                
                updateYesButton();
            }
        });
                  
                
                
                
                yesButton.setOnAction(e->{
                     selection=props.getProperty(PropertyType.YES);
                     dialog.close();
                         });
                 cancelButton.setOnAction(e->{
                     selection=props.getProperty(PropertyType.CANCEL);
                     dialog.close();});
                 
                 
                 directoryChooser.setOnAction(e->{
                     directory.clear();
                   DirectoryChooser fc = new DirectoryChooser();
                   fc.setInitialDirectory(new File(PATH_EXPORT));
                   fc.setTitle(props.getProperty(LOAD_WORK_TITLE));
                   
                   //how to choose directory????
                   File selectedDirectory = fc.showDialog(stage);
                   directory.setText(selectedDirectory.getAbsolutePath());
                    dir = selectedDirectory.getAbsolutePath();
                     updateYesButton();
                 });
                 
                 dataFileChooser.setOnAction(e->{
                      FileChooser fc = new FileChooser();
                   fc.setInitialDirectory(new File(PATH_raw_map_data));
                   fc.setTitle(props.getProperty(LOAD_WORK_TITLE));
                    File dataFile = fc.showOpenDialog(stage);
                    dataPath.setText(dataFile.getAbsolutePath());
                    filePath=dataFile.getAbsolutePath();
                    updateYesButton();
                 });
                 
                 dialog.setTitle(props.getProperty(PropertyType.ChangeMapNameDialog_TITLE));
                 Pane p = new Pane(gridPane);
                 Scene dialogScene = new Scene(p);
                 dialog.setScene(dialogScene);
                dialog.showAndWait();
      
 }
      private void updateYesButton() {
          boolean flag= false;
          String name = regionName.getText();
          String file = dataPath.getText();
          String dir = directory.getText();
          
          if(name.isEmpty()||file.isEmpty()||dir.isEmpty()||oldRegionName.equals(name)){
              flag=true;
          }
          yesButton.setDisable(flag);
     }
     
      public Button getDirectoryChooser(){return directoryChooser;}
      public Button getDataFileChooser(){return dataFileChooser;}
      public void SetFileChosserDisable(){
        this.getDirectoryChooser().setDisable(true);
        this. getDataFileChooser().setDisable(true);
      }
     public void setSelection(String sel) {
        this.selection=sel;
    }
    public String getSelection() {
        return selection;
    }
    public String getDir() {
        return dir;
    }
    public void setDir(String d) {
        this.dir=d;
    }
    
    public void setLabelStyle(Label label){
        label.getStylesheets().add(props.getProperty(PropertyType.CSS_PATH));
        label.getStyleClass().add(props.getProperty(PropertyType.FONT));
    }        
    
public String getFilePath(){return filePath;}

public String getRegionName() {
        return regionName.getText();
    }
 
public AppTemplate getAppTemplate(){return null;}

}
