/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvme.gui;

import java.io.File;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import rvme.PropertyType;
import rvme.data.DataManager;
import rvme.data.DesignMap;
import saf.AppTemplate;
import static saf.settings.AppPropertyType.LOAD_ICON;
import static saf.settings.AppPropertyType.LOAD_WORK_TITLE;

/**
 *
 * @author admin
 */
public class SubRegionDialog {
    public static final String FILE_PROTOCOL = "file:";
    public static final String PATH_IMAGES = "./images/";
    AppTemplate app;
    Stage stage;
    public PropertiesManager props;

    Button yesButton;
    Button cancelButton;
    Button Next;
    Button Previous;
    Button learderImageChooser =new Button();;
    Button flagImageChooser =new Button();;
    
    String leaderPath;
    String flagPath;
    
    Label LregionName;
    Label Lcapital;
    Label LleaderName;
    Label Lflag;
    Label LleaderImage;
//    public ArrayList<SubRegion> tableList ;
    public int currentIndex;
    
    TextField regionName;
    TextField capital;
    TextField leaderName;
    public String selection;
    
    
    public SubRegionDialog(AppTemplate ap){
        stage=ap.getGUI().getWindow();
        this.regionName=new TextField();
        this.capital=new TextField();
        this.leaderName=new TextField();
        app=ap;
    }
    public void showDialog() {
        props = PropertiesManager.getPropertiesManager();
        final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        GridPane gridPane = new GridPane();

        //dialogScene.getStylesheets().add("tdlm/css/tdlm_style.css");
        gridPane.getStylesheets().add(props.getProperty(PropertyType.CSS_PATH));
        gridPane.getStyleClass().add(props.getProperty(PropertyType.GRID_PANE));

        //make the add items table
        yesButton = new Button(props.getProperty(PropertyType.YES));
        cancelButton = new Button(props.getProperty(PropertyType.CANCEL));
        
        Next = ((Workspace)app.getWorkspaceComponent()).getNextBt();
        Previous = ((Workspace)app.getWorkspaceComponent()).getPreBt();
//        Next = initChildButton(PropertyType.NEXT_REGION_ICON.toString(), PropertyType.NEXT_BUTTON_TOOLTIP.toString(), false);
//        Previous =initChildButton(PropertyType.PREVIOUS_REGION_ICON.toString(), PropertyType.PREVIOUS_BUTTON_TOOLTIP.toString(), false);
//        

        //initial Label
        LregionName = new Label(props.getProperty(PropertyType.LregionName));
        Lcapital = new Label(props.getProperty(PropertyType.Lcapital));
        LleaderName = new Label(props.getProperty(PropertyType.LleaderName));
        Lflag = new Label(props.getProperty(PropertyType.Lflag));
        LleaderImage = new Label(props.getProperty(PropertyType.LleaderImage));

        //set Label style from css file
        setLabelStyle(LregionName);
        setLabelStyle(Lcapital);
        setLabelStyle(LleaderName);
        setLabelStyle(Lflag);
        setLabelStyle(LleaderImage);

        yesButton = new Button(props.getProperty(PropertyType.YES));
        cancelButton = new Button(props.getProperty(PropertyType.CANCEL));
        
            
        
	// LOAD THE ICON FROM THE PROVIDED FILE
        String imag = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(LOAD_ICON.toString());
        Image buttonImage = new Image(imag);
	
	// NOW MAKE THE BUTTON
        flagImageChooser.setGraphic(new ImageView(buttonImage));
        learderImageChooser.setGraphic(new ImageView(buttonImage));
        
        
        String flagImaP=FILE_PROTOCOL + flagPath ;
        String leaderImaP=FILE_PROTOCOL + leaderPath ;
            
        Image image = new Image(flagImaP);
        ImageView iv= new ImageView();
        iv.setFitHeight(60);
        iv.setFitWidth(80);
        iv.setImage(image);
        gridPane.add(iv, 1, 3);
        
        
        Image image2 = new Image(leaderImaP);
        
        ImageView iv2= new ImageView();
        iv2.setFitHeight(60);
        iv2.setFitWidth(80);
        iv2.setImage(image2);
        gridPane.add(iv2, 1, 4);
        
        
//        StackPane s =new StackPane();
//        s.getChildren().addAll(iv);
        
        
        gridPane.add(LregionName, 0, 0);
        gridPane.add(regionName, 1, 0);

        gridPane.add(Lcapital, 0, 1);
        gridPane.add(capital, 1, 1);

        gridPane.add(LleaderName, 0, 2);
        gridPane.add(leaderName, 1, 2);

        gridPane.add(Lflag, 0, 3);
        
        gridPane.add(flagImageChooser, 2, 3);
//
        gridPane.add(LleaderImage, 0, 4);
        
        gridPane.add(learderImageChooser, 2, 4);
        
        HBox hb= new HBox();
        hb.getChildren().addAll(Next,Previous);
        gridPane.add(hb, 0, 5);
//        gridPane.add(Previous, 0, 5);
        
        gridPane.add(yesButton, 0, 6);
        gridPane.add(cancelButton, 1, 6);

        GridPane.setRowSpan(yesButton, 2);
        GridPane.setRowSpan(cancelButton, 2);

        yesButton.setOnAction(e -> {
            selection = props.getProperty(PropertyType.YES);
            dialog.close();
        });
        cancelButton.setOnAction(e -> {
            selection = props.getProperty(PropertyType.CANCEL);
            dialog.close();
        });

                   flagImageChooser.setOnAction(e->{
                   FileChooser fc = new FileChooser();
                   fc.setInitialDirectory(new File(PATH_IMAGES));
                   fc.setTitle(props.getProperty(LOAD_WORK_TITLE));
                   File selectedDirectory = fc.showOpenDialog(stage);
                   
                   DesignMap designMap=((DataManager)app.getDataComponent()).getDesignMap();
                   String flaP=FILE_PROTOCOL + selectedDirectory.getAbsolutePath();
                   System.out.println(flaP);
                   Image flagI = new Image(flaP);
                   
                    ImageView flagIV= new ImageView();
                    flagIV.setFitHeight(60);
                    flagIV.setFitWidth(80);
                    flagIV.setImage(flagI);
                    gridPane.getChildren().remove(iv);
                    gridPane.getChildren().remove(flagIV);
                    gridPane.add(flagIV, 1, 3);
//                   directory.setText(selectedDirectory.getAbsolutePath());
//                    dir = selectedDirectory.getAbsolutePath();
//                     updateYesButton();
                 });
                 
               learderImageChooser.setOnAction(e->{
                      FileChooser fc = new FileChooser();
                   fc.setInitialDirectory(new File(PATH_IMAGES));
                   fc.setTitle(props.getProperty(LOAD_WORK_TITLE));
                    File dataFile = fc.showOpenDialog(stage);
                    
                    String flaP=FILE_PROTOCOL + dataFile.getAbsolutePath();
                   System.out.println(flaP);
                   Image leaderI = new Image(flaP);
                   
                    ImageView leaderIV= new ImageView();
                    leaderIV.setFitHeight(60);
                    leaderIV.setFitWidth(80);
                    leaderIV.setImage(leaderI);
//                    gridPane.getRowConstraints().get(4).;
                    gridPane.getChildren().remove(iv2);
                    gridPane.getChildren().remove(leaderIV);
                    gridPane.add(leaderIV, 1, 4);
                 });
        
        
        dialog.setTitle(props.getProperty(PropertyType.SubRegionDialog_TITLE));
        Scene dialogScene = new Scene(gridPane, 430, 450);
        dialog.setScene(dialogScene);
        dialog.showAndWait();

    }
    
     public Button initChildButton(String icon, String tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	
	// LOAD THE ICON FROM THE PROVIDED FILE
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(icon);
        Image buttonImage = new Image(imagePath);
	
	// NOW MAKE THE BUTTON
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip));
        button.setTooltip(buttonTooltip);
	
	// AND RETURN THE COMPLETED BUTTON
        return button;
    }
     
    public void setSelection(String sel) {
        this.selection = sel;
    }

    public String getSelection() {
        return selection;
    }
    
    public void setRegionName(String name){regionName.setText(name);}
    public void setCapital(String c){capital.setText(c);}
    public void setLeaderName(String l){leaderName.setText(l);}
    public String getRegionName() {
        return regionName.getText();
    }
    public String getCapital() {
        return capital.getText();
    }

    public String getLeaderName() {
        return leaderName.getText();
    }

    public void setLabelStyle(Label label) {
        label.getStylesheets().add(props.getProperty(PropertyType.CSS_PATH));
        label.getStyleClass().add(props.getProperty(PropertyType.FONT));
    }

}