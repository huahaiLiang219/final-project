package rvme.data;

import java.util.ArrayList;
import java.util.List;


public class CountryJSON {

	int NUMBER_OF_SUBREGIONS;
	
	ArrayList<SubRegions> SUBREGIONS;
	
	public CountryJSON() {
		// TODO Auto-generated constructor stub
	}

	public int getNUMBER_OF_SUBREGIONS() {
		return NUMBER_OF_SUBREGIONS;
	}

	public void setNUMBER_OF_SUBREGIONS(int NUMBER_OF_SUBREGIONS) {
		this.NUMBER_OF_SUBREGIONS = NUMBER_OF_SUBREGIONS;
	}

	public ArrayList<SubRegions> getSUBREGIONS() {
		return SUBREGIONS;
	}

	public void setSUBREGIONS(ArrayList<SubRegions> SUBREGIONS) {
		this.SUBREGIONS = SUBREGIONS;
	}
	
	
}
