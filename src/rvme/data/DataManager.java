package rvme.data;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import rvme.PropertyType;
import rvme.gui.Workspace;
import saf.components.AppDataComponent;
import saf.AppTemplate;
import saf.ui.AppGUI;

/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @version 1.0
 */
public class DataManager implements AppDataComponent {
    // FIRST THE THINGS THAT HAVE TO BE SAVED TO FILES
    public PropertiesManager props  = PropertiesManager.getPropertiesManager();
    String filepath;
    SimpleDoubleProperty scaleValue;
    double x=0;
    double y=0;
    public double translateX=0;
    public double translateY=0;
    public Button Next= new Button(props.getProperty(PropertyType.Next));
    public Button Previous= new Button(props.getProperty(PropertyType.Previous));
    // THESE ARE THE ITEMS IN THE TODO LIST
    ObservableList<ToDoItem> items;
    ProgressBar progressBar ;
    ObservableList<SubRegionForTable> subRegion;
    
    DesignMap designMap;
    CountryJSON countryJSON;
    
    StackPane leftp;
    SimpleDoubleProperty p;
    Pane mapPane;
    StackPane groupPan;
    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;
    
    public DataManager(){}
    /**
     * THis constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public DataManager(AppTemplate initApp) throws Exception {
	// KEEP THE APP FOR LATER
	app = initApp;
        progressBar=  new ProgressBar();
        p = new SimpleDoubleProperty(0);
//        Next = new Button(props.getProperty(PropertyType.Next));
//        Previous = new Button(props.getProperty(PropertyType.Previous));
//        System.out.println((DataManager)app.getDataComponent());
//        mapPane=(StackPane)(((SplitPane)((VBox)app.getWorkspaceComponent().getWorkspace().getChildren().get(1)).getChildren().get(0)).getItems().get(0));
        scaleValue = new SimpleDoubleProperty(1);
        setBarListener();
    }
    public void addBar(){
        progressBar.setProgress(0);
       app.getGUI().getAppPane().setBottom(progressBar);
    }
    public void setBarListener(){
        p.addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
               progressBar.setProgress(p.get());
            }
        });
    }
    public AppTemplate getApp(){return app;}
    public void setP(double value){
        p.set(value);
    }
    
    public void removeBar(){
        app.getGUI().getAppPane().setBottom(null);
    }
    
     public double getX(){return x;}
        public double getY(){return y;}
        public void setX(double wid){x=wid;}
        public void setY(double hei){x=hei;}
    public Pane getMapPane(){
//        return mapPane ;
         mapPane=((Pane) ((SplitPane)((VBox)app.getWorkspaceComponent().getWorkspace().getChildren().get(1)).getChildren().get(0)).getItems().get(0)) ;
         return mapPane;
    }
    
    public StackPane getLeftp() {
        System.out.println("2");
        leftp=((StackPane) ((SplitPane)((VBox)app.getWorkspaceComponent().getWorkspace().getChildren().get(1)).getChildren().get(0)).getItems().get(0));
        System.out.println("3");
        return leftp;
    }
    public void setMapPane(Pane mapPane){this.mapPane=mapPane;}
    
    public AppGUI getGui(){
    return app.getGUI();
    }
    public ObservableList<ToDoItem> getItems() {
	return items;
    }
    public ObservableList<SubRegionForTable> getSubRegionForTable() {
	return subRegion;
    }
    private void autoScale(){
        scaleValue.addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                mapPane.setScaleX(scaleValue.get());
                mapPane.setScaleY(scaleValue.get());
            }
        });
    }
    public String getFilePath() {
        return this.filepath;
    }
    public void setFilePath(String filePath) {
         this.filepath=filePath;
    }
    
    public void addItem(ToDoItem item) {
        items.add(item);
    }
    public void setItem(ObservableList<ToDoItem> items) {
        this.items=items;
    }
    public void addSubRegionForTable(SubRegionForTable sb) {
        subRegion.add(sb);
    }
    public void setSubRegionForTable(ObservableList<SubRegionForTable> sb) {
        this.subRegion=sb;
    }
    
    public void setScale(double value){
        scaleValue.set(value);
    }
    public double getScaleValue(){
        return scaleValue.get();
    } 
    //design map
    public DesignMap getDesignMap(){return designMap;}
    public void setDesignMap(DesignMap dm){this.designMap=dm;}
    
    //CountryJSON countryJSON;
    public CountryJSON getCountryJSON (){return  countryJSON;}
    public void setCountryJSON(CountryJSON countryJSON){this.countryJSON=countryJSON;}
    /**
     * 
     */
    @Override
    public void reset() {
        getMapPane().getChildren().clear();
        getSubRegionForTable().clear();
//        sp.setTranslateX(0);
//        sp.setTranslateY(0);
//        sp.setLayoutX(0);
//        sp.setLayoutY(0);
//        ((StackPane)(sp.getItems().get(0))).getChildren().clear();
    }

    
}
