/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvme.data;

/**
 *
 * @author admin
 */
public class colorInfo {
    double blue;
    double green;
    double red;
    double brightness;
    double hue;
    double saturation;
    boolean opaque; 
    double opacity;
    public double getBlue(){return blue;}
    public double getGreen(){return green;}
    public double getRed(){return red;}
    public double getBrightness(){return brightness;}
    public double getHue(){return hue;}
    public double getSaturation(){return saturation;}
    public double getOpacity(){return opacity;}
    public boolean getOpaque(){return opaque;}
    
    public void setBlue(double blue){this.blue=blue;}
    public void setGreen(double green){this.green=green;}
    public void setRed(double red){this.red=red;}
    public void setBrightness(double brightness){this.brightness=brightness;}
    public void setHue(double hue){this.hue=hue;}
    public void setSaturation(double saturation ){this.saturation=saturation;}
    public void setOpacity(double opacity){this.opacity=opacity;}
    public void setOpaque(boolean opaque){this.opaque=opaque;}
    
}



