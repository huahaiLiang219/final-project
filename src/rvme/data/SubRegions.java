package rvme.data;
import java.util.ArrayList;
import java.util.List;



    //This is for loading jason file 
public class SubRegions {

	private int NUMBER_OF_SUBREGION_POLYGONS;
	
	ArrayList<ArrayList<Polygon>> SUBREGION_POLYGONS;
	
	public SubRegions() {
		// TODO Auto-generated constructor stub
	}

	public int getNUMBER_OF_SUBREGION_POLYGONS() {
		return NUMBER_OF_SUBREGION_POLYGONS;
	}

	public void setNUMBER_OF_SUBREGION_POLYGONS(int NUMBER_OF_SUBREGION_POLYGONS) {
		this.NUMBER_OF_SUBREGION_POLYGONS = NUMBER_OF_SUBREGION_POLYGONS;
	}

	public ArrayList<ArrayList<Polygon>> getSUBREGION_POLYGONS() {
		return SUBREGION_POLYGONS;
	}

	public void setSUBREGION_POLYGONS(ArrayList<ArrayList<Polygon>> SUBREGION_POLYGONS) {
		this.SUBREGION_POLYGONS = SUBREGION_POLYGONS;
	}


	
}
