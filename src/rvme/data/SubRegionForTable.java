/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvme.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author admin
 */
public class SubRegionForTable {
    StringProperty nameForTable = new SimpleStringProperty();
    StringProperty capitalForTable= new SimpleStringProperty();
    StringProperty leaderForTable= new SimpleStringProperty();
    int red;
    int green;
    int blue;
    
    public SubRegionForTable(){}
    public SubRegionForTable(String rn,String cap,String ln ){
    this.nameForTable.set(rn);
        this.capitalForTable.set(cap);
        this.leaderForTable.set(ln);
    
    }
    public int getRed(){return red;}
    public int getGreen(){return green;}
    public int getBlue(){return blue;}
    public void setRGB(int r, int g,int  b){
        this.red=r;
        this.green=g;
        this.blue=b;
    }
    public String getNameForTable() {
        return nameForTable.get();
    }

    public void setNameForTable(String value) {
        nameForTable.set(value);
    }
    
    public String getCapitalForTable() {
        return capitalForTable.get();
    }

    public void setCapitalForTable(String value) {
        capitalForTable.set(value);
    }
    public String getLeaderForTable() {
        return leaderForTable.get();
    }

    public void setLeaderForTable(String value) {
        leaderForTable.set(value);
    }
}
