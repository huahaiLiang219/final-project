/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvme.data;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
/**
 *
 * @author admin
 */
     //This is for SubRegion Dialog
public class SubRegion {
     String name;
     String capital;
     String leader;
     String leaderImagePath;
     String flagImagePath;
     
//    StringProperty nameForTable = new SimpleStringProperty();
//    StringProperty capitalForTable= new SimpleStringProperty();
//    StringProperty leaderForTable= new SimpleStringProperty();
     
    private int red;
    private int green;
    private int blue;
    
    public String selection;
    
    public SubRegion(String rn,String cap,String ln,String li,String fi,int r,int g,int b){
        this.name=rn;
        this.capital=cap;
        this.leader=ln;
//        this.nameForTable.set(rn);
//        this.capitalForTable.set(cap);
//        this.leaderForTable.set(ln);
        
        this.leaderImagePath=li;
        this.flagImagePath=fi;
        this.red=r;
        this.green=g;
        this.blue=b;
        
    }

    public SubRegion() {
        
    }
    
//    public String getNameForTable() {
//        return nameForTable.get();
//    }
//
//    public void setNameForTable(String value) {
//        nameForTable.set(value);
//    }
//    
//    public String getCapitalForTable() {
//        return capitalForTable.get();
//    }
//
//    public void setCapitalForTable(String value) {
//        capitalForTable.set(value);
//    }
//    public String getLeaderForTable() {
//        return leaderForTable.get();
//    }
//
//    public void setLeaderForTable(String value) {
//        leaderForTable.set(value);
//    }
    public void setRGB(int r, int g,int b){
            setRed(r);
            setGreen(g);
            setBlue(b);
    }
    public int getRed() {
		return red;
	}

	public void setRed(int red) {
		this.red = red;
	}

	public int getGreen() {
		return green;
	}

	public void setGreen(int green) {
		this.green = green;
	}

	public int getBlue() {
		return blue;
	}

	public void setBlue(int blue) {
		this.blue = blue;
	}
    
    public String getName(){return name;}
    public String getCapital(){return capital;}
    public String getLeader(){return leader;}
    public String getLeaderImagePath(){return leaderImagePath;}
    public String getFlagImagePath(){return flagImagePath;}
    
//    
    public void setName(String region){this.name=region;}
    public void setCapital(String cap){this.capital=cap;}
    public void setLeader(String leader){this.leader=leader;}
    public void setLeaderImagePath(String leaderimage){this.leaderImagePath=leaderimage;}
    public void setFlagImagePath(String flagImage){this.flagImagePath=flagImage;}
    
    
    public void setSelection(String sel) {
        this.selection=sel;
    }
    public String getSelection() {
        return selection;
    }
}
