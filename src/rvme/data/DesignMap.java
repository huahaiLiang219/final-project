/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvme.data;

import java.util.ArrayList;
import javafx.scene.paint.Color;

/**
 *
 * @author admin
 */
public class DesignMap {
    boolean subregions_have_capitals;
    boolean subregions_have_flags;
    boolean subregions_have_leaders;
    
    String polygonsPath;
    String anthemPath;
//    SubRegion subRegion= new SubRegion();
    Color backgroundColor;
    Color borderColor;
    String regionName ;
    String parentDirectory; 
    
    String leaderImagePath;
    String flagImagePath;
    
    ArrayList flagImageLocation=new ArrayList();
    ArrayList leaderImageLocation=new ArrayList();
    ArrayList scrollMapLocation=new ArrayList();
    
    ArrayList<SubRegion> tableList = new ArrayList();
    
    double width;
    double height;
    double zoomLevel;
    
    int BackgroundRed;
    int BackgroundGreen;
    int BackgroundBlue;
    
    int BorderRed ;
    int BorderGreen ;
    int BorderBlue;
    double borderThickness;
    
    public DesignMap(){}
    public DesignMap(String regionName,String parentDirectory,String polygonsPath){
            this.regionName=regionName;
            this.parentDirectory=parentDirectory;
            this.polygonsPath=polygonsPath;
    
    }
    public ArrayList getFlagImageLocation(){return flagImageLocation; }
    public void setFlagImageLocation(ArrayList List){ flagImageLocation = List;}
    
    public ArrayList getLeaderImageLocation(){return leaderImageLocation; }
    public void setLeaderImageLocation(ArrayList List){ leaderImageLocation = List;}
    
    public ArrayList getScrollMapLocation(){return scrollMapLocation; }
    public void setScrollMapLocation(ArrayList List){ scrollMapLocation = List;}
    
    public void setParentDirectory(String p){this.parentDirectory=p;}
    public String getParentDirectory(){return this.parentDirectory;}
    
    public void setLeaderImagePath(String p){this.leaderImagePath=p;}
    public String getLeaderImagePath(){return this.leaderImagePath;}
    
    public void setFlagImagePath(String p){this.flagImagePath=p;}
    public String getFlagImagePath(){return this.flagImagePath;}
    //tableLIst
    public ArrayList<SubRegion> getTableList(){return tableList;}
    public void setTableList(ArrayList<SubRegion> list){ this.tableList=list;}
    //getter
    public String getPolygonsPath(){return polygonsPath;}
//    public SubRegion getSubRegion(){return subRegion;}
    
//    public Color getBackgroundColor(){return backgroundColor;}
//    public Color getBorderColor(){return borderColor;}
    public String getRegionName(){return regionName;}
    public String getAnthemPath(){return anthemPath;}
    public void setAnthemPath(String AnthemPath){this.anthemPath=AnthemPath;}
    
    
    //width and height and zoomlevel
    public double getWidth(){return width;}
    public double getHeight(){return height;}
    public double getZoomLevel(){return zoomLevel;}
    public double getBorderThickness(){return borderThickness;}
    
    public void setWidth(double w){this.width=w;}
    public void setBorderThickness(double bt){this.borderThickness=bt;}
    public void setHeight(double h){this.height=h;}
    public void setZoomLevel(double z){this.zoomLevel=z;}
    
    //Background color and border color
    public int getBorderRed(){return BorderRed;}
    public int getBorderGreen(){return BorderGreen;}
    public int getBorderBlue(){return BorderBlue;}
    
    public int getBackgroundRed(){return BackgroundRed;}
    public int getBackgroundGreen(){return BackgroundGreen;}
    public int getBackgroundBlue(){return BackgroundBlue;}
    
    public void setBorderRed(int r){this.BorderRed=r;}
    public void setBorderGreen(int g){this.BorderGreen=g;}
    public void setBorderBlue(int b){this.BorderBlue=b;}
    
    public void setBackgroundRed(int r){this.BackgroundRed=r;}
    public void setBackgroundGreen(int g){this.BackgroundGreen=g;}
    public void setBackgroundBlue(int b){this.BackgroundBlue=b;}
    
    public void setBorderColor(int r,int g,int b){
        this.BorderRed=r;
        this.BorderGreen=g;
        this.BorderBlue=b;
    }
    
    public void setBackgroundColor(int r,int g,int b ){
        this.BackgroundRed=r;
        this.BackgroundGreen=g;
        this.BackgroundBlue=b;
    }
    //setter
    public void setPolygonsPath(String path){this.polygonsPath=path;}
//    public void setSubRegion(String rn,String cap,String ln,String leaderImage,String flagImage){
//        subRegion.setRegionName(rn);
//        subRegion.setCapital(cap);
//        subRegion.setLeaderName(ln);
//    }
    
    public void setRegionName(String regionName){this.regionName=regionName;}
//    public void setBackgroundColor(int r, int g,int b){
//     backgroundColor = Color.rgb( r,  g, b);
//    }
//    public void setBorderColor(int r, int g,int b){
//     borderColor = Color.rgb(r,  g, b);
//    }
//    public void setTableList(ArrayList<SubRegion> tableList){this.tableList=tableList;}
    
    
    public boolean isSubregions_have_capitals() {
		return subregions_have_capitals;
	}

	public void setSubregions_have_capitals(boolean subregions_have_capitals) {
		this.subregions_have_capitals = subregions_have_capitals;
	}

	public boolean isSubregions_have_flags() {
		return subregions_have_flags;
	}

	public void setSubregions_have_flags(boolean subregions_have_flags) {
		this.subregions_have_flags = subregions_have_flags;
	}

	public boolean isSubregions_have_leaders() {
		return subregions_have_leaders;
	}

	public void setSubregions_have_leaders(boolean subregions_have_leaders) {
		this.subregions_have_leaders = subregions_have_leaders;
	}
    
    
}
