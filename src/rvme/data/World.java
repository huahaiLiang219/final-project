package rvme.data;

import java.util.ArrayList;


public class World {

	private String name;
	private boolean subregions_have_capitals;
	private boolean subregions_have_flags;
	private boolean subregions_have_leaders;
	private ArrayList<subRegionForWorld> subregions;
	
	public World() {
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isSubregions_have_capitals() {
		return subregions_have_capitals;
	}

	public void setSubregions_have_capitals(boolean subregions_have_capitals) {
		this.subregions_have_capitals = subregions_have_capitals;
	}

	public boolean isSubregions_have_flags() {
		return subregions_have_flags;
	}

	public void setSubregions_have_flags(boolean subregions_have_flags) {
		this.subregions_have_flags = subregions_have_flags;
	}

	public boolean isSubregions_have_leaders() {
		return subregions_have_leaders;
	}

	public void setSubregions_have_leaders(boolean subregions_have_leaders) {
		this.subregions_have_leaders = subregions_have_leaders;
	}

	public ArrayList<subRegionForWorld> getSubregions() {
		return subregions;
	}

	public void setSubregions(ArrayList<subRegionForWorld> subregions) {
		this.subregions = subregions;
	}

	@Override
	public String toString() {
		return "World [name=" + name + ", subregions_have_capitals="
				+ subregions_have_capitals + ", subregions_have_flags="
				+ subregions_have_flags + ", subregions_have_leaders="
				+ subregions_have_leaders + ", subregions=" + subregions + "]";
	}
	
	
}
