/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvme;

/**
 *
 * @author McKillaGorilla
 */
public enum PropertyType {
    WORKSPACE_HEADING_LABEL,
    ZOOM_SLIDER_LABEL,
    CHANGE_BORDER_THICKNESS_LABEL,
    ChangeMapNameDialog_TITLE,
    BGC_LABEL,
    BDC_LABEL,
    
    DETAILS_HEADING_LABEL,
    NAME_PROMPT,
    OWNER_PROMPT,

    ITEMS_HEADING_LABEL,
    
    ADD_IMAGE_ICON,
    REMOVE_IMAGE_ICON,
    CHANGE_MAP_NAME_ICON,
    NEXT_REGION_ICON,
    PREVIOUS_REGION_ICON,
    
    CHANGE_BACKGROUD_COLOR_ICON,
    CHANGE_BORDER_COLOR_ICON,
    CHANGE_BORDER_THICKNESS_ICON,
    
    REASSIGH_MAP_COLORS_ICON,
    PLAY_ANTHEM_BUTTON_ICON,
    CHANGE_MAP_DIMENSIONS_ICON,
    
    NEXT_BUTTON_TOOLTIP,
    PREVIOUS_BUTTON_TOOLTIP,
    
    
    ADD_IMAGE_BUTTON_TOOLTIP,
    REMOVE_IMAGE_BUTTON_TOOLTIP,
    CHANGE_MAP_NAME_BUTTON_TOOLTIP,
    
    CHANGE_BACKGROUD_COLOR_BUTTON_TOOLTIP,
    CHANGE_BORDER_COLOR_BUTTON_TOOLTIP,
    CHANGE_BORDER_THICKNESS_BUTTON_TOOLTIP,
    
    REASSIGH_MAP_COLORS_BUTTON_TOOLTIP,
    PLAY_ANTHEM_BUTTON_TOOLTIP,
    CHANGE_MAP_DIMENSIONS_BUTTON_TOOLTIP,
    
     LRegionName,
     Ldirectory,
     LdataPath,
    
     LregionName,
     Lcapital,
     LleaderName,
     Lflag,
     LleaderImage,
     
     HEIGHT,
     WIDTH,
    
    DimisionsDialog_TITLE,
    SubRegionDialog_TITLE,
    NewMapDialog_TITLE,
    
    REGIONNAME_COLUMN_HEADING,
    CAPITAL_COLUMN_HEADING,
    LEADERNAME_COLUMN_HEADING,
    
    YES,
    NO,
    CANCEL,
    TABLE_TITLE,
    Previous,
    Next,
    
    CSS_PATH,
    FONT,
    GRID_PANE,
}
