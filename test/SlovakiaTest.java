
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import rvme.data.DesignMap;
import rvme.test_bed.TestLoad;
import rvme.test_bed.TestSave;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author admin
 */
public class SlovakiaTest {
    @Test
    public void testSlovakia() {
         System.out.println("createSlovakia");
         DesignMap designMapFromSave =TestSave.createAndorra();
         DesignMap designMapFromLoad =TestLoad.loadAndorraMap();
         
         
         
         System.out.println("---compare Region Name---");
         assertEquals(designMapFromLoad.getRegionName(), designMapFromSave.getRegionName());
         
         System.out.println("---compare subregions 1st 2nd 3th---");
         assertEquals(designMapFromLoad.getTableList().get(2).getName(), designMapFromSave.getTableList().get(2).getName());
         assertEquals(designMapFromLoad.getTableList().get(2).getLeader(), designMapFromSave.getTableList().get(2).getLeader());
        
         System.out.println("---compare backgroundcolor rgb---");
         assertEquals(designMapFromLoad.getBorderRed(), designMapFromSave.getBorderRed());
         assertEquals(designMapFromLoad.getBackgroundGreen(), designMapFromSave.getBackgroundGreen());
         assertEquals(designMapFromLoad.getBackgroundBlue(), designMapFromSave.getBackgroundBlue());
         
         System.out.println("---compare PolygonsPath---");
         assertEquals(designMapFromLoad.getPolygonsPath(), designMapFromSave.getPolygonsPath());
         
         System.out.println("---compare AnthemPath---");
         assertEquals(designMapFromLoad.getAnthemPath(), designMapFromSave.getAnthemPath());
        
         System.out.println("---compare FlagImagePath---");
         assertEquals(designMapFromLoad.getFlagImagePath(), designMapFromSave.getFlagImagePath());
        
         System.out.println("---compare BorderThickness---");
         assertEquals(designMapFromLoad.getHeight(),  designMapFromSave.getHeight(), 0);
   
    
    }
}
